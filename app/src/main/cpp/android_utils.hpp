//
// Created by kinsh on 01-11-2019.
//

#ifndef MORFORM_ANDROID_UTILS_HPP
#define MORFORM_ANDROID_UTILS_HPP

#include <jni.h>
#include <string>
#include <fstream>
#include <android/log.h>
#include <opencv2/opencv.hpp>

using Contour = std::vector<cv::Point>;
using FloatContour = std::vector<cv::Point2f>;

class androidbuf: public std::streambuf {
public:
    enum { bufsize = 128 }; // ... or some other suitable buffer size
    androidbuf() { this->setp(buffer, buffer + bufsize - 1); }
private:
    int overflow(int c) {
        if (c == traits_type::eof()) {
            *this->pptr() = traits_type::to_char_type(c);
            this->sbumpc();
        }
        return this->sync() ? traits_type::eof(): traits_type::not_eof(c);
    }
    int sync() {
        int rc = 0;
        if (this->pbase() != this->pptr()) {
            __android_log_print(ANDROID_LOG_DEBUG,
                                "Native",
                                "%s",
                                std::string(this->pbase(),
                                            this->pptr() - this->pbase()).c_str());
            rc = 0;
            this->setp(buffer, buffer + bufsize - 1);
        }
        return rc;
    }
    char buffer[bufsize];
};

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr) return "";

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes, env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte* pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *)pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}

jobject to_ArrayList(JNIEnv *env, FloatContour contour) {
    jclass java_util_ArrayList = static_cast<jclass>(env->NewGlobalRef(env->FindClass("java/util/ArrayList")));
    jclass android_graphics_Point = static_cast<jclass>(env->NewGlobalRef(env->FindClass("android/graphics/Point")));
    auto array_ctor = env->GetMethodID(java_util_ArrayList, "<init>", "(I)V");
    jobject array;
    if(array_ctor != nullptr) {
        array = env->NewObject(java_util_ArrayList, array_ctor, contour.size());
        for(cv::Point p : contour) {
            auto ctor = env->GetMethodID(android_graphics_Point, "<init>", "(II)V");
            if(ctor != nullptr) {
                jobject element = env->NewObject(android_graphics_Point, ctor, (jint)p.x, (jint)p.y);
                env->CallBooleanMethod(array, env->GetMethodID(java_util_ArrayList, "add", "(Ljava/lang/Object;)Z"), element);
                env->DeleteLocalRef(element);
            }
        }
    }
    return array;
}

Contour from_ArrayList(JNIEnv *env, jobject contour) {
    Contour array;
    jclass java_util_ArrayList = static_cast<jclass>(env->NewGlobalRef(env->FindClass("java/util/ArrayList")));
    jclass android_graphics_Point = static_cast<jclass>(env->NewGlobalRef(env->FindClass("android/graphics/Point")));
    jint len = env->CallIntMethod(contour, env->GetMethodID (java_util_ArrayList, "size", "()I")); array.reserve(static_cast<unsigned int>(len));
    for (jint i=0; i<len; i++) {
        jobject element = env->CallObjectMethod(contour, env->GetMethodID(java_util_ArrayList, "get", "(I)Ljava/lang/Object;"), i);
        jint x = env->GetIntField(element, env->GetFieldID(android_graphics_Point,"x","I"));
        jint y = env->GetIntField(element, env->GetFieldID(android_graphics_Point,"y","I"));
        array.push_back({x, y});
        env->DeleteLocalRef(element);
    }
    return array;
}

#endif //MORFORM_ANDROID_UTILS_HPP
