#include "android_utils.hpp"

Contour operator+(Contour original, cv::Point offset)
{
    Contour translated(original.size());
    for(size_t i = 0; i < original.size(); i++)
        translated[i] = original[i] + offset;
    return translated;
}
Contour operator*(Contour original, double scale)
{
    if(scale<1)
    {
        cv::Rect rct = cv::boundingRect(original);

        cv::Point rct_offset(-rct.tl().x, -rct.tl().y);
        Contour dc_contour = original + rct_offset;

        Contour dc_contour_scale(dc_contour.size());

        for (size_t i = 0; i < dc_contour.size(); i++)
            dc_contour_scale[i] = dc_contour[i] * scale;

        cv::Rect rct_scale = cv::boundingRect(dc_contour_scale);

        cv::Point offset((rct.width - rct_scale.width) / 2, (rct.height - rct_scale.height) / 2);
        offset -= rct_offset;

        return dc_contour_scale + offset;
    }
    else
    {
        Contour scaled = original;
        for(cv::Point& pt : scaled) { pt.x*=scale; pt.y*=scale; }
        return scaled;
    }
}
FloatContour order_contour(Contour contour)
{
    FloatContour rect; int min_i_s = 0, max_i_s = 0, min_i_d = 0, max_i_d = 0;
    double min_v_s = 999.0, max_v_s = 0.0, min_v_d = 999.0, max_v_d = 0.0, v; size_t i=0;
    for(const cv::Point& p : contour)
    {
        v = p.x+p.y;
        if(v<min_v_s) { min_i_s = i; min_v_s = v; }
        if(v>max_v_s) { max_i_s = i; max_v_s = v; }
        v = p.y-p.x;
        if(v<min_v_d) { min_i_d = i; min_v_d = v; }
        if(v>max_v_d) { max_i_d = i; max_v_d = v; }
        i++;
    }
    rect = {contour[min_i_s], contour[min_i_d], contour[max_i_s], contour[max_i_d]};
    return rect;
}

constexpr double MIN_EXTENT = 30.0;
constexpr int MAX_HEIGHT = 900;

extern "C" JNIEXPORT void JNICALL
Java_com_kv_morform_utilities_ImageUtils_rotate(JNIEnv *env, jclass root_class, jstring path, jdouble angle) {
    std::cout.rdbuf(new androidbuf); std::cout.setf(std::ios::unitbuf);
    std::string file_path = jstring2string(env, path);
    cv::Mat original = cv::imread(file_path);

    if(angle==90) {
        std::vector<int> compression_params = { cv::IMWRITE_JPEG_QUALITY, 100};
        cv::rotate(original, original, cv::ROTATE_90_CLOCKWISE); cv::imwrite(file_path, original, compression_params);
    } else if(angle==180) {
        std::vector<int> compression_params = { cv::IMWRITE_JPEG_QUALITY, 100};
        cv::rotate(original, original, cv::ROTATE_180); cv::imwrite(file_path, original, compression_params);
    } else {
        int h = original.size().height, w = original.size().height;
        cv::Mat rotation_matrix = cv::getRotationMatrix2D({w/2.0f, h/2.0f}, -angle, 1.0);

        float cos = static_cast<float>(abs(rotation_matrix.at<double>(0,0))), sin = static_cast<float>(abs(rotation_matrix.at<double>(0,1)));
        int new_w = int((h*sin)+(w*cos)), new_h = int((h*cos)+(w*sin));
        rotation_matrix.at<double>(0,2) += new_w/2.0 - w/2.0; rotation_matrix.at<double>(1,2) += new_h/2.0 - h/2.0;

        std::vector<int> compression_params = { cv::IMWRITE_JPEG_QUALITY, 100};
        cv::warpAffine(original, original, rotation_matrix, {new_w, new_h}); cv::imwrite(file_path, original, compression_params);
    }
}

extern "C" JNIEXPORT jobject JNICALL
Java_com_kv_morform_utilities_ImageUtils_getContour(JNIEnv *env, jclass clazz, jstring string) {
    std::string path = jstring2string(env, string);
    std::cout.rdbuf(new androidbuf); std::cout.setf(std::ios::unitbuf);

    cv::Mat original = cv::imread(path), resized, edges, processed;

    int width = original.size().width, height = original.size().height, ks = 3, new_height = MAX_HEIGHT;
    double ratio = width/(double)height, scale_factor = height/(double)new_height;

    resized = original;
    if(height>new_height)
    {
        cv::resize(original, resized, {static_cast<int>(ratio*new_height), new_height});
        width = resized.size().width; height = resized.size().height;
    }
    cv::cvtColor(resized, resized, cv::COLOR_BGR2GRAY);
    cv::bilateralFilter(resized, edges, ks+2, 2*ks+4, (ks/2)+1);
    cv::GaussianBlur(edges, edges, {ks+2, ks+2}, 0);
    cv::Canny(edges, edges, 75, 225, ks);

    std::vector<Contour> contours; Contour approx_contour;
    cv::findContours(edges, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE, {0,0});

    std::sort(contours.begin(), contours.end(), [](const Contour& c1, const Contour& c2){ return cv::contourArea(c1)>cv::contourArea(c2); });

    for(const auto& c : contours)
    {
        double len = cv::arcLength(c, true);
        cv::approxPolyDP(c,approx_contour,0.02*len,true);
        if(approx_contour.size()==4) { break; }
    }

    double area = height*width;
    double extent = cv::contourArea(approx_contour)/area;
    double extent_percent = extent*100.0;
    if(extent_percent<MIN_EXTENT)
    {
        original = cv::imread(path); resized = original; width = resized.size().width; height = resized.size().height;
        if(height>new_height)
        {
            cv::resize(original, resized, {static_cast<int>(ratio*new_height), new_height});
            width = resized.size().width; height = resized.size().height;
        }
        cv::copyMakeBorder(resized, resized, 10, 10, 10, 10, cv::BORDER_CONSTANT, {0,0,0});
        cv::cvtColor(resized, resized, cv::COLOR_BGR2GRAY);
        cv::bilateralFilter(resized, edges, ks+2, 2*ks+4, (ks/2)+1);
        cv::GaussianBlur(edges, edges, {ks+2, ks+2}, 0);
        cv::Canny(edges, edges, 75, 225, ks);

        contours.clear();
        cv::findContours(edges, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE, {0,0});

        std::sort(contours.begin(), contours.end(), [](const Contour& c1, const Contour& c2){ return cv::contourArea(c1)>cv::contourArea(c2); });
        for(const auto& c : contours)
        {
            double len = cv::arcLength(c, true);
            cv::approxPolyDP(c,approx_contour,0.02*len,true);
            if(approx_contour.size()==4) { break; }
        }

        area = width*height;
        approx_contour = approx_contour + cv::Point(-10, -10);

        extent = cv::contourArea(approx_contour)/area; extent_percent = extent*100.0;
        if(extent_percent<MIN_EXTENT) { return nullptr; }
    }

    if(scale_factor>1) { approx_contour = approx_contour * scale_factor; processed = original; }
    auto ordered = order_contour(approx_contour);
    return to_ArrayList(env, ordered);
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_kv_morform_utilities_ImageUtils_processImage__Ljava_lang_String_2Ljava_util_ArrayList_2(JNIEnv *env, jclass clazz, jstring string, jobject contour) {
    std::string path = jstring2string(env, string);
    std::cout.rdbuf(new androidbuf); std::cout.setf(std::ios::unitbuf);
    std::cout<<"> Path selected: "+path<<"\n";

    cv::Mat original = cv::imread(path), resized, edges, processed = original;

    auto approx_contour = from_ArrayList(env, contour);
    for(const auto& p : approx_contour) { std::cout<<"("+std::to_string(p.x)+", "+std::to_string(p.y)+")"; }
    auto ordered = order_contour(approx_contour);

    double widthA = sqrt(pow((ordered[2].x-ordered[3].x),2)+pow((ordered[2].y-ordered[3].y),2)); // Bottom-Left and Bottom-Right
    double widthB = sqrt(pow((ordered[0].x-ordered[1].x),2)+pow((ordered[0].y-ordered[1].y),2)); // Top-Left and Top-Right
    float maxWidth = std::max(int(widthA), int(widthB));

    double heightA = sqrt(pow((ordered[0].x-ordered[3].x),2)+pow((ordered[0].y-ordered[3].y),2)); // Top-Left and Bottom-Left
    double heightB = sqrt(pow((ordered[2].x-ordered[1].x),2)+pow((ordered[2].y-ordered[1].y),2)); // Top-Right and Bottom-Right
    float maxHeight = std::max(int(heightA), int(heightB));

    std::cout<<"> Contour Image: Dimensions: "+std::to_string(maxWidth)+" by "+std::to_string(maxHeight)<<"\n";

    std::vector<cv::Point2f> new_contour = {{0,0},{maxWidth-1,0},{maxWidth-1,maxHeight-1},{0,maxHeight-1}};
    auto perspective = cv::getPerspectiveTransform(ordered, new_contour);
    cv::warpPerspective(processed, processed, perspective, {int(maxWidth), int(maxHeight)});
    std::cout<<"> Perspective adjustment complete. Performing final touches...";
    cv::cvtColor(processed, processed, cv::COLOR_BGR2GRAY);
    cv::adaptiveThreshold(processed, processed, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 7);

    size_t pos = path.find_last_of("\\/")+1, dot = path.rfind('.'); std::string save_path = path.substr(0,pos);
    std::string stem = path.substr(pos,dot-pos), extension; if(dot!=std::string::npos) extension = path.substr(dot+1);
    save_path += stem; save_path+="_processed"; save_path+="."; save_path+=extension;
    std::cout<<"> Saving to "+save_path<<"\n"; { std::ofstream(save_path.data()); }
    std::vector<int> compression_properties = { cv::IMWRITE_JPEG_QUALITY, 100 };
    cv::imwrite(save_path, processed, compression_properties);
    return env->NewStringUTF(save_path.data());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_kv_morform_utilities_ImageUtils_processImage__Ljava_lang_String_2(JNIEnv *env, jclass clazz, jstring string) {
    std::string path = jstring2string(env, string);
    std::cout.rdbuf(new androidbuf); std::cout.setf(std::ios::unitbuf);
    std::cout<<"> Path selected: "+path<<"\n";

    cv::Mat original = cv::imread(path), resized, edges, processed;

    int width = original.size().width, height = original.size().height, ks = 3, new_height = MAX_HEIGHT;
    double ratio = width/(double)height, scale_factor = height/(double)new_height;
    std::cout<<"> Size (original)  : "+std::to_string(width)+" by "+std::to_string(height)<<"\n";
    std::cout<<"> Aspect ratio     : "+std::to_string(ratio)+":1"<<"\n";
    std::cout<<"> Scale down factor: "+std::to_string(scale_factor)<<"\n";

    resized = original;
    if(height>new_height)
    {
        cv::resize(original, resized, {static_cast<int>(ratio*new_height), new_height});
        width = resized.size().width; height = resized.size().height;
        std::cout<<"> Size (rescale)   : "+std::to_string(width)+" by "+std::to_string(height)<<"\n";
    }
    cv::cvtColor(resized, resized, cv::COLOR_BGR2GRAY);
    cv::bilateralFilter(resized, edges, ks+2, 2*ks+4, (ks/2)+1);
    cv::GaussianBlur(edges, edges, {ks+2, ks+2}, 0);
    cv::Canny(edges, edges, 75, 225, ks);

    std::vector<Contour> contours; Contour approx_contour;
    cv::findContours(edges, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE, {0,0});

    std::sort(contours.begin(), contours.end(), [](const Contour& c1, const Contour& c2){ return cv::contourArea(c1)>cv::contourArea(c2); });

    for(const auto& c : contours)
    {
        double len = cv::arcLength(c, true);
        cv::approxPolyDP(c,approx_contour,0.02*len,true);
        if(approx_contour.size()==4) { break; }
    }

    double area = height*width;
    double extent = cv::contourArea(approx_contour)/area;
    double extent_percent = extent*100.0;
    std::cout<<"> Selected contour covers "+std::to_string(extent_percent)+"% of image.\n";
    if(extent_percent<MIN_EXTENT)
    {
        std::cout<<"> Useless contour selected. Retrying with borders...\n";
        original = cv::imread(path); resized = original; width = resized.size().width; height = resized.size().height;
        if(height>new_height)
        {
            cv::resize(original, resized, {static_cast<int>(ratio*new_height), new_height});
            width = resized.size().width; height = resized.size().height;
            std::cout<<"> Size (rescale)   : "+std::to_string(width)+" by "+std::to_string(height)<<"\n";
        }
        cv::copyMakeBorder(resized, resized, 10, 10, 10, 10, cv::BORDER_CONSTANT, {0,0,0});
        cv::cvtColor(resized, resized, cv::COLOR_BGR2GRAY);
        cv::bilateralFilter(resized, edges, ks+2, 2*ks+4, (ks/2)+1);
        cv::GaussianBlur(edges, edges, {ks+2, ks+2}, 0);
        cv::Canny(edges, edges, 75, 225, ks);

        contours.clear();
        cv::findContours(edges, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE, {0,0});

        std::sort(contours.begin(), contours.end(), [](const Contour& c1, const Contour& c2){ return cv::contourArea(c1)>cv::contourArea(c2); });
        for(const auto& c : contours)
        {
            double len = cv::arcLength(c, true);
            cv::approxPolyDP(c,approx_contour,0.02*len,true);
            if(approx_contour.size()==4) { break; }
        }

        area = width*height;
        approx_contour = approx_contour + cv::Point(-10, -10);

        extent = cv::contourArea(approx_contour)/area; extent_percent = extent*100.0;
        std::cout<<"> New Selected contour covers "+std::to_string(extent_percent)+"% of image.\n";
        if(extent_percent<MIN_EXTENT) { std::cout<<"> Another useless contour!\n"; return env->NewStringUTF(""); }
        else { std::cout<<"> Possible document selected.\n"; }
    }
    else { std::cout<<"> Possible document selected.\n"; }

    if(scale_factor>1) { approx_contour = approx_contour * scale_factor; processed = original; }
    auto ordered = order_contour(approx_contour);

    double widthA = sqrt(pow((ordered[2].x-ordered[3].x),2)+pow((ordered[2].y-ordered[3].y),2)); // Bottom-Left and Bottom-Right
    double widthB = sqrt(pow((ordered[0].x-ordered[1].x),2)+pow((ordered[0].y-ordered[1].y),2)); // Top-Left and Top-Right
    float maxWidth = std::max(int(widthA), int(widthB));

    double heightA = sqrt(pow((ordered[0].x-ordered[3].x),2)+pow((ordered[0].y-ordered[3].y),2)); // Top-Left and Bottom-Left
    double heightB = sqrt(pow((ordered[2].x-ordered[1].x),2)+pow((ordered[2].y-ordered[1].y),2)); // Top-Right and Bottom-Right
    float maxHeight = std::max(int(heightA), int(heightB));

    std::cout<<"> Contour Image: Dimensions: "+std::to_string(maxWidth)+" by "+std::to_string(maxHeight)<<"\n";

    std::vector<cv::Point2f> new_contour = {{0,0},{maxWidth-1,0},{maxWidth-1,maxHeight-1},{0,maxHeight-1}};
    auto perspective = cv::getPerspectiveTransform(ordered, new_contour);
    cv::warpPerspective(processed, processed, perspective, {int(maxWidth), int(maxHeight)});
    std::cout<<"> Perspective adjustment complete. Performing final touches...";
    cv::cvtColor(processed, processed, cv::COLOR_BGR2GRAY);
    cv::adaptiveThreshold(processed, processed, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 7);

    size_t pos = path.find_last_of("\\/")+1, dot = path.rfind('.'); std::string save_path = path.substr(0,pos);
    std::string stem = path.substr(pos,dot-pos), extension; if(dot!=std::string::npos) extension = path.substr(dot+1);
    save_path += stem; save_path+="_processed"; save_path+="."; save_path+=extension;
    std::cout<<"> Saving to "+save_path<<"\n"; { std::ofstream(save_path.data()); }
    std::vector<int> compression_properties = { cv::IMWRITE_JPEG_QUALITY, 100 };
    cv::imwrite(save_path, processed, compression_properties);
    return env->NewStringUTF(save_path.data());
}

extern "C" JNIEXPORT void JNICALL
Java_com_kv_morform_utilities_ImageUtils_adjustRatio(JNIEnv *env, jclass clazz, jstring string, jdouble ratio) {
    std::string path = jstring2string(env, string);
    std::cout.rdbuf(new androidbuf); std::cout.setf(std::ios::unitbuf);
    std::cout<<"> Path selected: "+path<<"\n";

    cv::Mat original = cv::imread(path), processed;
    int width = original.size().width, height = original.size().height, delta_x = 0, delta_y = 0;
    double aspect = height/static_cast<double>(width); int new_width = width, new_height = height;
    if(aspect<=ratio) { new_height = static_cast<int>(width*ratio);  }
    else { new_width = static_cast<int>(height/ratio); }
    delta_y = abs(new_height-height)/2; delta_x = abs(new_width-width)/2;
    std::cout<<"> Size (original)  : "+std::to_string(width)+" by "+std::to_string(height)<<"\n";
    std::cout<<"> Size (adjust  )  : "+std::to_string(new_width)+" by "+std::to_string(new_height)<<"\n";
    std::cout<<"Border dimensions: ("+std::to_string(delta_x)+", "+std::to_string(delta_y)+")\n";
    std::cout<<"Old ratio: "+std::to_string(aspect)<<"\n"; std::cout<<"New ratio: "+std::to_string(ratio)<<"\n";
    cv::copyMakeBorder(original, processed, delta_y, delta_y, delta_x, delta_x, cv::BORDER_CONSTANT, {255,255,255});

    std::cout<<"> Saving to "+path<<"\n"; { std::ofstream(path.data()); }
    std::vector<int> compression_properties = { cv::IMWRITE_JPEG_QUALITY, 100 };
    cv::imwrite(path, processed, compression_properties);
}