package com.kv.morform.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.kv.morform.R;
import com.kv.morform.activities.TemporaryImageActivity;
import com.kv.morform.utilities.ImageUtils;

import java.util.ArrayList;
import java.util.Locale;

public class CropFragment extends Fragment {
    private CropView crop_area;

    private String image_path;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_crop, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        final TemporaryImageActivity activity = ((TemporaryImageActivity)getActivity());
        final RelativeLayout layout = (RelativeLayout) getView();
        if(activity!=null && layout!=null) {
            try {
                image_path = ImageUtils.getUnprocessed(activity.getSelectedImage()); crop_area = layout.findViewById(R.id.crop_area);
                ImageButton accept = getView().findViewById(R.id.btn_accept), reject = getView().findViewById(R.id.btn_reject);
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View view) { activity.onCropSuccess(); }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override public void onClick(View view) { activity.onCropFailure(); }
                });
                if(image_path!=null) {
                    BitmapFactory.Options options = new BitmapFactory.Options(); options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(image_path, options); crop_area.setImageBitmap(bitmap);
                } else { reject.performClick(); }
                layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        try {
                            crop_area.initialize();
                            crop_area.setCropContour(ImageUtils.getContour(image_path));
                        } catch (Exception e) {
                            Log.e(CropView.class.getName(), "error: "+e); e.printStackTrace();
                        }
                        layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            } catch (Exception e) {
                Log.e(TemporaryImageActivity.class.getName(), "error: "+e); e.printStackTrace();
                activity.finish();
            }
        }
    }

    public String saveCrop() {
        try {
            int i = 0;
            ArrayList<Point> points = crop_area.getCropContour();
            for(Point p : points) {
                i++;
                Log.d(CropFragment.class.getName(), String.format(Locale.US, "| %d | Point #%d = (%d, %d)", i, i, p.x, p.y));
            }
            String new_path = ImageUtils.getProcessedImage(image_path, points);
            Log.d(CropView.class.getName(), new_path);
            return new_path;
        } catch (Exception e) {
            Toast.makeText(getContext(),"error: "+e, Toast.LENGTH_SHORT).show();
            Log.e(CropFragment.class.getName(), "error: "+e); e.printStackTrace();
            return image_path;
        }
    }
}
