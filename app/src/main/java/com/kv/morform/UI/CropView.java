package com.kv.morform.UI;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;

import com.kv.morform.R;

import java.util.ArrayList;

public class CropView extends AppCompatImageView {
    private static final String TAG = CropView.class.getName();

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG), circle_paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    ArrayList<Point> points = new ArrayList<>(); Point center;

    int circle_index = -1; boolean dirty = true;
    int cropBorderColor = Color.argb(255, 66, 135, 245);
    int width = 0, height = 0, bitmap_width = 0, bitmap_height = 0, delta_x, delta_y;
    float bitmap_aspect = 0.0f, aspect = 0.0f;

    public void initialize() {
        width = getWidth(); height = getHeight(); center = new Point(width/2,height/2); Drawable drawable = getDrawable();
        bitmap_width = drawable.getIntrinsicWidth(); bitmap_height = drawable.getIntrinsicHeight();
        width-=(getPaddingStart()+getPaddingEnd()); height-=(getPaddingBottom()+getPaddingTop());
        bitmap_aspect = bitmap_height/(float)bitmap_width; aspect = height/(float) width;
        int expected_bitmap_height, expected_bitmap_width;

        Log.d(TAG, "Aspect ratios: Bitmap = "+bitmap_aspect+", View = "+aspect);
        if(aspect>=bitmap_aspect) {expected_bitmap_height = (int) (width*bitmap_aspect); expected_bitmap_width = width; }
        else { expected_bitmap_height = height; expected_bitmap_width = (int) (height/bitmap_aspect); }
        //int expected_bitmap_width = (int)
        delta_y = (height-expected_bitmap_height)/2;
        delta_x = (width-expected_bitmap_width)/2; dirty = false;
    }

    private void initializeCropArea() {
        if(points.isEmpty()) {
            try {
                int sy = (delta_y > 0) ? (height - delta_y) : height;
                int sx = (delta_x > 0) ? (width - delta_x) : width;
                points.add(new Point(getPaddingStart()+(delta_x>0?delta_x:0),getPaddingTop()+(delta_y>0?delta_y:0)));
                points.add(new Point(sx+getPaddingStart(),getPaddingTop()+(delta_y>0?delta_y:0)));
                points.add(new Point(sx+getPaddingStart(), getPaddingTop()+sy));
                points.add(new Point(getPaddingStart()+(delta_x>0?delta_x:0), getPaddingTop()+sy));
            } catch(Exception e) {
                Log.e(TAG, "error: "+e); e.printStackTrace();
            }
        }
    }
    private boolean withinBounds(float x, float y) {
        int sy = (delta_y > 0) ? (height - delta_y) : height; int sx = (delta_x > 0) ? (width - delta_x) : width;
        return (x>=(delta_x>0?delta_x:0) && x<=(sx+getPaddingStart()+getPaddingEnd())) && (y>=(delta_y>0?delta_y:0) && y<=(sy+getPaddingTop()+getPaddingBottom()));
    }
    private boolean inCircle(Point point, float x, float y) {
        return Math.pow(x-point.x, 2)+Math.pow((y-point.y), 2) <= 6400.0;
    }
    private void selectAreaInFocus(Canvas canvas) {
        Paint fillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setColor(Color.argb(128,0,0,0));
        if(!points.isEmpty()) {
            Path path = new Path(); path.reset(); path.setFillType(Path.FillType.INVERSE_EVEN_ODD);
            path.moveTo(points.get(0).x, points.get(0).y);
            for(int i=1;i<points.size();i++) { path.lineTo(points.get(i).x, points.get(i).y); }
            path.lineTo(points.get(0).x, points.get(0).y);
            canvas.drawPath(path, fillPaint);
        }
    }
    private void setColor(AttributeSet attrs) {
        TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.CropView, 0, 0);
        try { cropBorderColor = array.getColor(R.styleable.CropView_cropBorderColor, Color.argb(255, 66, 135, 245)); }
        finally { array.recycle(); }
    }
    private boolean drawAreaUnderCircle(Canvas canvas) {
        try {
            Point pt = new Point(points.get(circle_index));
            float scale = delta_x==0?bitmap_width/(float)width:bitmap_height/(float)height;
            pt.x = pt.x - getPaddingStart() - delta_x;
            pt.y = pt.y - getPaddingTop() - delta_y;
            pt.x = (int)(pt.x*scale); pt.y = (int)(pt.y*scale);
            Bitmap original = ((BitmapDrawable)getDrawable()).getBitmap();
            Bitmap area = Bitmap.createBitmap(original, pt.x-20, pt.y-20, 80, 80);
            Bitmap scaled = Bitmap.createScaledBitmap(area, 240, 240, true);
            Bitmap result = Bitmap.createBitmap(240, 240, Bitmap.Config.ARGB_8888);

            Canvas result_canvas = new Canvas(result);

            final int color = 0xff424242; final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, 240, 240);

            paint.setAntiAlias(true); result_canvas.drawARGB(0, 0, 0, 0); paint.setColor(color);
            result_canvas.drawCircle(120, 120, 120, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            result_canvas.drawBitmap(scaled, rect, rect, paint);
            canvas.drawBitmap(result,center.x-120,center.y-120,null);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "error: "+e);
            e.printStackTrace(); return false;
        }
        //Canvas output = new Canvas(bordered); output.drawARGB(255,0,0,0); output.drawBitmap(original,50,50,null);
        //Log.d(TAG, "Crop Dimensions: "+bordered.getWidth()+" by "+bordered.getHeight());
    }

    public CropView(Context context) { super(context); }
    public CropView(Context context, AttributeSet attrs) { super(context, attrs); setColor(attrs); }
    public CropView(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle); setColor(attrs); }

    @SuppressLint("ClickableViewAccessibility")
    @Override public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_CANCEL: {
                Log.d(TAG, "Event was cancelled."); circle_index=-1; invalidate(); return false;
            }
            case MotionEvent.ACTION_UP: {
                Log.d(TAG, "Event Type: UP"); circle_index = -1;  invalidate(); return false;
            }
            case MotionEvent.ACTION_MOVE: {
                Log.d(TAG, "Event Type: MOVE");
                if(withinBounds(event.getX(), event.getY())) { if(circle_index!=-1) {
                    points.set(circle_index, new Point((int) event.getX(), (int) event.getY())); invalidate(); } }
                return true;
            }
            case MotionEvent.ACTION_DOWN: {
                Log.d(TAG, "Event Type: DOWN");
                if(withinBounds(event.getX(), event.getY())) {
                    for(int i=0;i<points.size();i++) {
                        if(inCircle(points.get(i), event.getX(), event.getY())) {
                            circle_index = i; invalidate(); break;
                        }
                    }
                }
                return true;
            }
            default: Log.d(TAG, "Unknown touch event."); return false;
        }
    }

    public void setCropContour(ArrayList<Point> contour) throws NullPointerException {
        if(contour==null || contour.size()!=4) return;
        if(!dirty) {
            float scale = delta_x==0?bitmap_width/(float)width:bitmap_height/(float)height;
            points = new ArrayList<>(contour);
            for(Point p : points) {
                p.x = (int)(p.x/scale); p.y = (int)(p.y/scale);
                p.x = p.x + getPaddingStart() + delta_x; p.y = p.y + getPaddingTop() + delta_y;
            }
            dirty = true; invalidate();
        } else throw new NullPointerException("View not rendered yet. Cannot calculate bounds. (instance==null)");
    }
    public ArrayList<Point> getCropContour() {
        ArrayList<Point> contour = new ArrayList<>(points);
        float scale = delta_x==0?bitmap_width/(float)width:bitmap_height/(float)height;
        points = new ArrayList<>(contour);
        for(Point p : contour) {
            p.x = p.x - getPaddingStart() - delta_x; p.y = p.y - getPaddingTop() - delta_y;
            p.x = (int)(p.x*scale); p.y = (int)(p.y*scale);
        }
        return contour;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas); if(dirty) { initialize(); }
        paint.setStyle(Paint.Style.STROKE); paint.setStrokeWidth(14); paint.setColor(cropBorderColor);
        if(points.isEmpty()) { initializeCropArea(); } selectAreaInFocus(canvas);
        for(int i=0;i<points.size();i++) {
            canvas.drawLine(points.get(i).x,points.get(i).y,points.get(i==3?0:i+1).x,points.get(i==3?0:i+1).y,paint);
        }
        for(int i=0;i<points.size();i++) {
            circle_paint.setColor(Color.argb(255,255,255,255)); circle_paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(points.get(i).x, points.get(i).y, 40, circle_paint);
            circle_paint.setColor(cropBorderColor); circle_paint.setStyle(Paint.Style.STROKE); circle_paint.setStrokeWidth(14);
            canvas.drawCircle(points.get(i).x, points.get(i).y, 40, circle_paint);
        }
        if(circle_index!=-1) {
            boolean success = drawAreaUnderCircle(canvas);
            if(success) {
                circle_paint.setColor(Color.argb(255,255,255,255)); circle_paint.setStrokeWidth(7); circle_paint.setStyle(Paint.Style.STROKE);
                canvas.drawCircle(center.x, center.y, 120, circle_paint);
                canvas.drawLine(center.x, center.y-30, center.x, center.y+30, circle_paint);
                canvas.drawLine(center.x-30, center.y, center.x+30, center.y, circle_paint);
            }
        }
    }
}
