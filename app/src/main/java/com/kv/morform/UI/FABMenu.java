package com.kv.morform.UI;

import android.app.Application;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class FABMenu {
    private ArrayList<FABMenuItem> items = new ArrayList<>();
    private FloatingActionButton base;
    private Application app;
    private boolean inflated = false;
    private OnFABItemSelectedListener fabItemSelectedListener;
    private OnFABMenuInflatedListener fabMenuInflatedListener;

    private void refreshPositions() {
        Log.d(FABMenu.class.getName(), "Refreshing Angles...");
        double sector = 90.0 / items.size(); int dx; double angle = sector/2.0; int i = 0;
        for(FABMenuItem item : items) {
            dx = item.button.getWidth(); dx*=(1.0/Math.sin(Math.toRadians(sector))); dx-=40; dx/=2;
            item.dx = (int) ( dx*Math.cos(Math.toRadians(angle)) );
            item.dy = (int) ( dx*Math.sin(Math.toRadians(angle)) );
            Log.d(FABMenu.class.getName(), "Angle = "+angle+". Translate "+item.dx+" by "+item.dy);
            angle+=sector; i++;
        }
    }

    public interface OnFABItemSelectedListener { void onFABItemSelected(FABMenuItem item); }
    public interface OnFABMenuInflatedListener { void onFABMenuInflated(boolean inflated); }

    public FABMenu(Application app, final FloatingActionButton base) {
        this.app = app; this.base = base;
        this.base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = false;
                if(inflated) { for(FABMenuItem item : items) { success = item.hide(); } }
                else { for(FABMenuItem item : items) { success = item.show(); } }
                if(success) { inflated = !inflated; fabMenuInflatedListener.onFABMenuInflated(inflated); Log.d(FABMenu.class.getName(),"Inflated? = "+inflated); }
            }
        });
    }
    public void rebind() {
        base.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = false;
                if(inflated) { for(FABMenuItem item : items) { success = item.hide(); } }
                else { for(FABMenuItem item : items) { success = item.show(); } }
                if(success) { inflated = !inflated; fabMenuInflatedListener.onFABMenuInflated(inflated); Log.d(FABMenu.class.getName(),"Inflated? = "+inflated); }
            }
        });
    }
    public void addItem(FloatingActionButton button) {
        final FABMenuItem fabItem = new FABMenuItem(app, button);
        fabItem.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deflate();
                fabItemSelectedListener.onFABItemSelected(fabItem);
            }
        });
        items.add(fabItem);
    }
    public void inflate() {
        refreshPositions(); for(FABMenuItem item : items) { item.repositionRelativeTo(base); }
    }
    public void deflate() {
        if(inflated) { for(FABMenuItem item : items) { item.hide(); } }
        inflated = false; fabMenuInflatedListener.onFABMenuInflated(false);
    }
    public void add(FloatingActionButton... items) {
        for(FloatingActionButton btn : items) { addItem(btn); }
    }
    public FloatingActionButton getRoot() { return base; }
    public void setOnFABItemSelectedListener(OnFABItemSelectedListener fabItemSelectedListener) { this.fabItemSelectedListener = fabItemSelectedListener; }
    public void setOnFABMenuInflatedListener(OnFABMenuInflatedListener fabMenuInflatedListener) { this.fabMenuInflatedListener = fabMenuInflatedListener; }
}
