package com.kv.morform.UI;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Application;
import android.util.Log;
import android.util.TypedValue;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class FABMenuItem {
    public FloatingActionButton button; int dx, dy;
    public Application app; boolean is_shown = false;
    public FABMenuItem(Application app, FloatingActionButton button) {
        this.app = app; this.button = button;
    }
    public void repositionRelativeTo(FloatingActionButton base) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) button.getLayoutParams(),
                rootParams = (CoordinatorLayout.LayoutParams) base.getLayoutParams();
        params.setMargins(rootParams.leftMargin + (base.getWidth()-button.getWidth())/2,
                rootParams.topMargin + (base.getHeight()-button.getHeight())/2,
                rootParams.rightMargin + (base.getWidth()-button.getWidth())/2,
                rootParams.bottomMargin + (base.getHeight()-button.getHeight())/2);
        button.setLayoutParams(params);
        Log.d(FABMenuItem.class.getName(),"Repositioning button...");
    }
    public boolean show() {
        if(!is_shown) {
            int[] loc = new int[2]; button.getLocationOnScreen(loc);
            int DX = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dx, app.getApplicationContext().getResources().getDisplayMetrics());
            int DY = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dy, app.getApplicationContext().getResources().getDisplayMetrics());
            ObjectAnimator animX = ObjectAnimator.ofFloat(button, "x",loc[0], loc[0]-DX);
            ObjectAnimator animY = ObjectAnimator.ofFloat(button, "y",loc[1], loc[1]-DY);
            ObjectAnimator fade = ObjectAnimator.ofFloat(button, "alpha",0.0f, 1.0f);
            AnimatorSet anim_set = new AnimatorSet();
            anim_set.playTogether(new ObjectAnimator[]{ animX, animY, fade });
            anim_set.setDuration(100);
            anim_set.addListener(new Animator.AnimatorListener() {
                @Override public void onAnimationStart(Animator animator) { is_shown = false; }
                @Override public void onAnimationEnd(Animator animator) { is_shown = true; }
                @Override public void onAnimationCancel(Animator animator) { }
                @Override public void onAnimationRepeat(Animator animator) { }
            });
            anim_set.start(); button.setClickable(true); return true;
        }
        return false;
        //Log.d(FABMenuItem.class.getName(),"Showing buttons...");
    }
    public boolean hide() {
        if(is_shown) {
            int[] loc = new int[2]; button.getLocationOnScreen(loc);
            int DX = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dx, app.getApplicationContext().getResources().getDisplayMetrics());
            int DY = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dy, app.getApplicationContext().getResources().getDisplayMetrics());
            ObjectAnimator animX = ObjectAnimator.ofFloat(button, "x",loc[0], loc[0]+DX);
            ObjectAnimator animY = ObjectAnimator.ofFloat(button, "y",loc[1], loc[1]+DY);
            ObjectAnimator fade = ObjectAnimator.ofFloat(button, "alpha",1.0f, 0.0f);
            AnimatorSet anim_set = new AnimatorSet();
            anim_set.playTogether(new ObjectAnimator[]{ animX, animY, fade });
            anim_set.setDuration(100);
            anim_set.addListener(new Animator.AnimatorListener() {
                @Override public void onAnimationStart(Animator animator) { is_shown = true; }
                @Override public void onAnimationEnd(Animator animator) { is_shown = false; }
                @Override public void onAnimationCancel(Animator animator) { }
                @Override public void onAnimationRepeat(Animator animator) { }
            });
            anim_set.start(); button.setClickable(false); return true;
        }
        return false;
        //Log.d(FABMenuItem.class.getName(),"Hiding buttons...");
    }
}
