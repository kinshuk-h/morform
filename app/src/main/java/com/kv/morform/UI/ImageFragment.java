package com.kv.morform.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.chrisbanes.photoview.PhotoView;
import com.kv.morform.R;
import com.kv.morform.activities.TemporaryImageActivity;
import com.kv.morform.utilities.Constants;
import com.kv.morform.utilities.ImageUtils;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ImageFragment extends Fragment {
    private PhotoView image;
    private ImageButton accept, reject, rotate, crop;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_image, parent, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        final TemporaryImageActivity activity = ((TemporaryImageActivity) getActivity());
        RelativeLayout layout = (RelativeLayout) getView();
        if(activity !=null && layout !=null) {
            try {
                RecyclerView page_list = layout.findViewById(R.id.page_list);
                page_list.setAdapter(activity.getAdapter());
                page_list.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
                image = layout.findViewById(R.id.capture_image); loadImage(activity.getAdapter().getSelected());
                accept = layout.findViewById(R.id.btn_accept); reject = layout.findViewById(R.id.btn_reject);
                rotate = layout.findViewById(R.id.btn_rotate); crop = layout.findViewById(R.id.btn_crop);
                View.OnClickListener stateListener = new View.OnClickListener() { @Override public void onClick(View view) { activity.submit(view); } };
                accept.setOnClickListener(stateListener); reject.setOnClickListener(stateListener);
                rotate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ImageUtils.rotate(activity.getAdapter().getSelected(), 90);
                        activity.getAdapter().notifyItemChanged(activity.getAdapter().getSelectedIndex());
                        loadImage(activity.getAdapter().getSelected());
                    }
                });
                crop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.loadCropFragment();
                    }
                });
            } catch (Exception e) {
                Log.e(TemporaryImageActivity.class.getName(), "error: "+e); e.printStackTrace();
                activity.finish();
            }
        }
    }

    public void loadImage(String image_path) {
        BitmapFactory.Options options = new BitmapFactory.Options(); options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(image_path, options); image.setImageBitmap(bitmap);
    }
}
