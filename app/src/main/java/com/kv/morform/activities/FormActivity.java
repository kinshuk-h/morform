package com.kv.morform.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.material.snackbar.Snackbar;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import com.kv.morform.R;
import com.kv.morform.adapters.FormFieldAdapter;
import com.kv.morform.database.MetaDBHelper;
import com.kv.morform.main.Form;
import com.kv.morform.main.FormField;
import com.kv.morform.utilities.BasicUtils;
import com.kv.morform.utilities.Constants;
import com.kv.morform.utilities.LocationTextStrategy;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class FormActivity extends AppCompatActivity {

    private static final String TAG = FormActivity.class.getName();
    MetaDBHelper dbHelper;

    private Menu menu; private PDFView pdf_view; private Form form;
    private RelativeLayout no_form_view;
    private DrawerLayout drawer;
    private FormFieldAdapter adapter;

    private boolean night_mode = false, dark_theme = true;
    private int current_page = 0;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.form_menu, menu); this.menu = menu;
        try { form.getForm(); }
        catch (Exception e) {
            menu.findItem(R.id.night_mode).setEnabled(false);
            menu.findItem(R.id.show_data_entries).setEnabled(false);
            menu.findItem(R.id.show_fields).setEnabled(false);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.night_mode: {
                night_mode = !night_mode;
                if(night_mode) {
                    item.setIcon(dark_theme?R.drawable.ic_night_mode_disable_dark:R.drawable.ic_night_mode_disable_light);
                    try {
                        pdf_view.fromFile(form.getForm()).swipeHorizontal(true).enableSwipe(true).scrollHandle(new DefaultScrollHandle(getApplicationContext()))
                                .enableAnnotationRendering(true).pageFling(true).defaultPage(current_page).spacing(5).nightMode(true)
                                .onPageChange(new OnPageChangeListener() {
                                    @Override public void onPageChanged(int page, int pageCount) { current_page = page; }
                                }).fitEachPage(true).pageFitPolicy(FitPolicy.BOTH).load();
                        pdf_view.setBackgroundColor(getResources().getColor(dark_theme?R.color.backgroundDark:R.color.backgroundLight));
                    }
                    catch (FileNotFoundException e) {
                        no_form_view.setVisibility(View.VISIBLE);
                        pdf_view.setVisibility(View.GONE);
                        menu.findItem(R.id.night_mode).setEnabled(false);
                        menu.findItem(R.id.show_data_entries).setEnabled(false);
                    }
                }
                else {
                    item.setIcon(dark_theme?R.drawable.ic_night_mode_enable_dark:R.drawable.ic_night_mode_enable_light);
                    try {
                        pdf_view.fromFile(form.getForm()).swipeHorizontal(true).enableSwipe(true).scrollHandle(new DefaultScrollHandle(getApplicationContext()))
                                .enableAnnotationRendering(true).pageFling(true).defaultPage(current_page).spacing(5).nightMode(false)
                                .onPageChange(new OnPageChangeListener() {
                                    @Override public void onPageChanged(int page, int pageCount) { current_page = page; }
                                }).fitEachPage(true).pageFitPolicy(FitPolicy.BOTH).load();
                        pdf_view.setBackgroundColor(getResources().getColor(dark_theme?R.color.backgroundDark:R.color.backgroundLight));
                    }
                    catch (FileNotFoundException e) {
                        no_form_view.setVisibility(View.VISIBLE);
                        pdf_view.setVisibility(View.GONE);
                        menu.findItem(R.id.night_mode).setEnabled(false);
                        menu.findItem(R.id.show_data_entries).setEnabled(false);
                    }
                }
                break;
            }
            case R.id.show_data_entries: {
                Intent with_intention = new Intent(this, FormDataActivity.class);
                with_intention.putExtra(Constants.Data.EXTRA_FORM, form);
                startActivity(with_intention); break;
            }
            case R.id.show_fields: {
                if(drawer.isDrawerOpen(GravityCompat.END)) { drawer.closeDrawer(GravityCompat.END); }
                else drawer.openDrawer(GravityCompat.END);
                break;
            }
            case R.id.share: {
                try {
                    Intent with_intention = new Intent(Intent.ACTION_SEND);
                    with_intention.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
                        with_intention.setDataAndType(FileProvider.getUriForFile(getApplicationContext(), "com.kv.morform.provider", form.getForm()), "application/pdf");
                        with_intention.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(getApplicationContext(), "com.kv.morform.provider", form.getForm()));
                    }
                    else {
                        with_intention.setDataAndType(Uri.fromFile(form.getForm()), "application/pdf");
                        with_intention.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(form.getForm()));
                    }
                    if(with_intention.resolveActivity(getPackageManager())!=null) {
                        startActivity(with_intention);
                    } else {
                        Toast.makeText(getApplicationContext(), "Can't send PDF. Search an app on the Play Store. ", Toast.LENGTH_SHORT).show();
                        with_intention = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/search?c=apps"));
                        if(with_intention.resolveActivity(getPackageManager())!=null) startActivity(with_intention);
                    }
                } catch (Exception e) { Log.e(TAG, "| X | error: "+e); e.printStackTrace(); }
                break;
            }
            case R.id.save_copy: {
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE); intent.setType("application/pdf");
                    intent.putExtra(Intent.EXTRA_TITLE, form.getName());
                    startActivityForResult(intent, Constants.Requests.SAVE_REQUEST);
                } else Toast.makeText(getApplicationContext(), "Storage access denied. Provide appropriate permission to continue.", Toast.LENGTH_SHORT).show();
                break;
            }
            case android.R.id.home: { onBackPressed(); break; }
            default: return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        dark_theme = prefs.getBoolean(Constants.Settings.DARK_THEME, true);
        setTheme(dark_theme?R.style.AppThemeDark:R.style.AppThemeLight);
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_form);

        Toolbar toolbar = findViewById(R.id.form_toolbar);
        setSupportActionBar(toolbar); toolbar.inflateMenu(R.menu.form_menu);

        pdf_view = findViewById(R.id.pdf_view);
        no_form_view = findViewById(R.id.no_form_view);

        dbHelper = new MetaDBHelper(this);

        Bundle data = getIntent().getExtras();
        if(data!=null) {
            try {
                form = (Form) data.get(Constants.Data.EXTRA_FORM); TextView form_name = findViewById(R.id.form_name);
                if(form!=null) {
                    form_name.setText(form.getName()); no_form_view.setVisibility(View.GONE); pdf_view.setVisibility(View.VISIBLE);
                    try {
                        pdf_view.fromFile(form.getForm()).swipeHorizontal(true).enableSwipe(true).scrollHandle(new DefaultScrollHandle(getApplicationContext()))
                                .enableAnnotationRendering(true).pageFling(true).defaultPage(current_page).spacing(5)
                                .onPageChange(new OnPageChangeListener() {
                                    @Override public void onPageChanged(int page, int pageCount) { current_page = page; }
                                }).fitEachPage(true).pageFitPolicy(FitPolicy.BOTH).load();
                        pdf_view.setBackgroundColor(getResources().getColor(dark_theme?R.color.backgroundDark:R.color.backgroundLight));
                    }
                    catch (FileNotFoundException e) {
                        Log.e(TAG, "| X | error: "+e); e.printStackTrace(); no_form_view.setVisibility(View.VISIBLE); pdf_view.setVisibility(View.GONE);
                    }
                }
            }
            catch (Exception e) { e.printStackTrace(); }
        }

        drawer = findViewById(R.id.form_layout);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                if (item.getItemId() == android.R.id.home) onBackPressed();
                return super.onOptionsItemSelected(item);
            }
        };
        mDrawerToggle.syncState();

        toolbar.setNavigationOnClickListener (new View.OnClickListener () {
            @Override public void onClick(View view) { onBackPressed(); }
        });

        mDrawerToggle.setDrawerIndicatorEnabled(false);
        ActionBar bar = getSupportActionBar(); if(bar!=null) bar.setDisplayHomeAsUpEnabled(true);

        // Initialize form fields
        adapter = new FormFieldAdapter(this, dark_theme);
        if(form!=null) {
            ArrayList<FormField> fields = form.parse();
            if(!fields.isEmpty()) {
                findViewById(R.id.empty_fields).setVisibility(View.GONE);
                findViewById(R.id.btn_tray).setVisibility(View.VISIBLE);
                for(FormField field : fields) adapter.pushBack(field);
            }
        }

        // Load fields into recycler view.
        RecyclerView field_list = findViewById(R.id.field_list);
        field_list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        field_list.setAdapter(adapter);

        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.dialog_show_text, null);
        TextView text = view.findViewById(R.id.text); text.setText(extractText());
        final AlertDialog dialog = new AlertDialog.Builder(FormActivity.this)
                .setTitle("Extracted Text")
                .setCancelable(true)
                .setView(view)
                .create();

        ImageButton clear = findViewById(R.id.btn_clear), save = findViewById(R.id.btn_save), ocr = findViewById(R.id.btn_ocr);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clearData(); Toast.makeText(getApplicationContext(), "Fields cleared successfully.", Toast.LENGTH_SHORT).show();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result_message = form.save(dbHelper, adapter.getFields());
                Toast.makeText(getApplicationContext(), result_message, Toast.LENGTH_SHORT).show();
            }
        });
        ocr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!extractText().isEmpty()) dialog.show();
                else Toast.makeText(getApplicationContext(), "Text extraction failed.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.END)) drawer.closeDrawer(GravityCompat.END);
        else finish();
    }

    String extractText() {
        if(form!=null) {
            try {
                StringBuilder parsedText=new StringBuilder();
                PdfReader reader = new PdfReader(form.getForm().toString());
                PdfReaderContentParser parser = new PdfReaderContentParser(reader);
                for(int i=1;i<=reader.getNumberOfPages();i++) {
                    TextExtractionStrategy strategy = parser.processContent(i, new LocationTextStrategy());
                    parsedText.append(strategy.getResultantText().trim()).append("\n");
                }
                reader.close();
                return parsedText.toString().trim();
            }
            catch (Exception e) { Log.e(TAG, "error: "+e); e.printStackTrace(); }
        } return "";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK) {
            if(requestCode==Constants.Requests.SAVE_REQUEST) {
                Uri path = data.getData();
                if(path!=null) {
                    String file_path = BasicUtils.extractPath(this, path);
                    if(file_path!=null) {
                        Log.d(TAG, "| ! | Destination Path: "+file_path);
                        try {
                            BasicUtils.copyFile(form.getForm().toString(), file_path);
                            Snackbar.make(drawer, "Form saved", Snackbar.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Save operation failed. Try again.", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "| X | error: "+e); e.printStackTrace();
                        }
                    }
                } else Toast.makeText(getApplicationContext(), "No file specified. Try again.", Toast.LENGTH_SHORT).show();
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }
}
