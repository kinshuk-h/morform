package com.kv.morform.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kv.morform.R;
import com.kv.morform.adapters.FormDataAdapter;
import com.kv.morform.adapters.FormFieldAdapter;
import com.kv.morform.database.MetaDBHelper;
import com.kv.morform.main.Form;
import com.kv.morform.main.FormField;
import com.kv.morform.main.Header;
import com.kv.morform.main.PlainText;
import com.kv.morform.utilities.AndroidUtils;
import com.kv.morform.utilities.BasicUtils;
import com.kv.morform.utilities.Constants;

import java.io.File;
import java.util.ArrayList;

public class FormDataActivity extends AppCompatActivity {
    final static String TAG = FormDataActivity.class.getName();

    private boolean dark_theme = true;
    private Form form;
    private FormDataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        dark_theme = prefs.getBoolean(Constants.Settings.DARK_THEME, true);

        setTheme(dark_theme?R.style.AppThemeDark:R.style.AppThemeLight);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_data);

        final MetaDBHelper dbHelper = new MetaDBHelper(this);

        Bundle data = getIntent().getExtras();
        if(data!=null) {
            form = (Form) data.get(Constants.Data.EXTRA_FORM);
            if(form!=null) {
                ImageView image = findViewById(R.id.form_image);
                TextView name = findViewById(R.id.form_name), size = findViewById(R.id.form_size), date = findViewById(R.id.created_date);
                name.setText(form.getName()); date.setText(form.getDateCreated());

                adapter = new FormDataAdapter(this, dark_theme);
                adapter.setOnDeleteListener(new FormDataAdapter.OnDeleteListener() {
                    @Override
                    public void onDelete(int id) {
                        dbHelper.deleteData(form.getIdentifier(), id);
                    }
                });
                adapter.setOnItemClickListener(new FormDataAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(ViewGroup parent, View v, int position) {
                        FormFieldAdapter fieldAdapter = new FormFieldAdapter(FormDataActivity.this, dark_theme);
                        View view = LayoutInflater.from(FormDataActivity.this).inflate(R.layout.layout_drawer,null);
                        view.findViewById(R.id.empty_fields).setVisibility(View.GONE);
                        Cursor row = dbHelper.getData(form.getIdentifier(), adapter.getItem(position)); row.moveToFirst();
                        ArrayList<FormField> fields = form.parse();
                        for(FormField field : fields) {
                            if(field instanceof Header) {} else if(field instanceof PlainText) {}
                            else {
                                field.fromString(row.getString(row.getColumnIndex(BasicUtils.getMangledName(field.getLabel()))));
                            }
                        }
                        for(FormField field : fields) {
                            fieldAdapter.pushBack(field);
                        }
                        RecyclerView field_list = view.findViewById(R.id.field_list);
                        field_list.setLayoutManager(new LinearLayoutManager(FormDataActivity.this, RecyclerView.VERTICAL, false));
                        field_list.setAdapter(fieldAdapter);
                        AlertDialog dialog = new AlertDialog.Builder(FormDataActivity.this).setTitle("Form Entry\n").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setView(view).setCancelable(true).create();
                        dialog.show();
                    }
                });
                adapter.addOnEmptyListener(new FormDataAdapter.OnEmptyListener() {
                    @Override
                    public void onEmpty() {
                        FormDataActivity.this.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
                    }
                });

                try {
                    File form_file = form.getForm();
                    size.setText(BasicUtils.fileSize(form_file.length()));
                    try (PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(form_file, ParcelFileDescriptor.MODE_READ_ONLY))) {
                        try(PdfRenderer.Page page = renderer.openPage(0)) {
                            Bitmap page_image = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);
                            page.render(page_image, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                            image.setBackgroundColor(Color.argb(255,255,255,255));
                            image.setImageBitmap(page_image);
                        }
                    }
                    catch (Exception e) {
                        Bitmap bitmap = AndroidUtils.toBitmap(getResources().getDrawable(dark_theme?R.drawable.ic_form_fill_dark:R.drawable.ic_form_fill_light));
                        Bitmap new_bitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), bitmap.getConfig());
                        Canvas canvas = new Canvas(new_bitmap);
                        canvas.drawColor(Color.parseColor(dark_theme?"#242424":"#DFDFDF"));
                        //canvas.drawBitmap(bitmap, 0,0,null);
                        image.setBackgroundColor(Color.parseColor(dark_theme?"#242424":"#DFDFDF"));
                        image.setImageBitmap(new_bitmap);
                    }
                }
                catch(Exception e) {
                    Log.e(TAG, "error: "+e); e.printStackTrace();
                }
                if(dbHelper.findTable(form.getIdentifier())) {
                    Cursor cursor = dbHelper.getData(form.getIdentifier());
                    for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()) {
                        adapter.pushBack(cursor.getInt(cursor.getColumnIndex("__ID__")));
                    }
                }

                if(adapter.getItemCount()>0) findViewById(R.id.empty_view).setVisibility(View.GONE);

                RecyclerView list = findViewById(R.id.data_list);
                list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
                list.setAdapter(adapter);
                return;
            }
        }
        Toast.makeText(getApplicationContext(), "There was a problem while retrieving data. Try again.", Toast.LENGTH_SHORT).show();
        finish();
    }
}
