package com.kv.morform.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.kv.morform.UI.FABMenu;
import com.kv.morform.UI.FABMenuItem;
import com.kv.morform.R;
import com.kv.morform.adapters.FormAdapter;
import com.kv.morform.database.MetaDBHelper;
import com.kv.morform.main.Form;
import com.kv.morform.utilities.BasicUtils;
import com.kv.morform.utilities.Constants;
import com.kv.morform.utilities.ImageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    final static String TAG = MainActivity.class.getName();
    Context themedContext; String file_path;

    private boolean dark_theme = true, select_mode = false, safe_delete = true;
    private int page_size = 0;

    FormAdapter form_adapter; MainViewModel data_model; FloatingActionButton load;
    FABMenu fab_menu; RecyclerView form_view; RelativeLayout empty_view; CoordinatorLayout main_layout;

    private MetaDBHelper metaDbHelper;

    /// Load native library on application startup.
    static {
        System.loadLibrary("native-lib");
        System.loadLibrary("opencv_java4");
    }

    private class PDFSaveTask extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override protected void onPreExecute() { progressDialog = ProgressDialog.show(MainActivity.this, "Creating PDF", "Resizing Images..."); }
        @Override protected void onProgressUpdate(String... params) { progressDialog.setMessage(params[0]); }
        @Override protected void onPostExecute(String result) { progressDialog.dismiss(); if(result!=null && !result.isEmpty()) { addForm(result); } }
        @Override protected String doInBackground(String... params) {
            try {
                String pdf_file = params[0];
                for(int i=1;i<params.length;i++) {
                    Log.d(TAG, String.format(Locale.US, "| %"+(params.length<11?"1":"2")+"d | Path: %s", i, params[i])); publishProgress("Resizing image "+i+"...");
                    ImageUtils.adjustRatio(params[i], Constants.pages[page_size].getHeight()/((double)Constants.pages[page_size].getWidth()));
                }
                Document document = new Document(); PdfWriter.getInstance(document, new FileOutputStream(pdf_file));
                document.setPageSize(Constants.pages[page_size]); document.setMargins(0,0,0,0); document.open();
                for(int i=1; i<params.length; i++) {
                    publishProgress("Adding page "+i+" to PDF...");
                    Image image = Image.getInstance(params[i]);
                    float scale = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - 0) / image.getWidth()) * 100;
                    image.scalePercent(scale); image.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                    document.add(image);
                }
                document.close(); publishProgress("Done!"); return pdf_file;
            } catch (Exception e) {
                Snackbar.make(main_layout, "An error occurred while creating a new form.", Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, "| X | error: "+e); e.printStackTrace(); return null;
            }
        }
    }

    private void setAdapterListeners() {
        final View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(safe_delete) {
                    DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int button) {
                            if(button==DialogInterface.BUTTON_POSITIVE) { for(int i=0;i<form_adapter.getItemCount();i++) {
                                if(form_adapter.getItem(i).isSelected()) { metaDbHelper.deleteTable(form_adapter.getItem(i).getIdentifier()); form_adapter.erase(i); i--; } } form_adapter.refresh(); }
                        }
                    };
                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Confirm Delete Forms").setCancelable(true)
                            .setMessage("You are about to delete "+form_adapter.getSelectedCount()+" form"+(form_adapter.getSelectedCount()>1?"s":"")
                                    +". Are you sure? Note that all form data will also be deleted.")
                            .setPositiveButton("DELETE", dialogListener).setNegativeButton("CANCEL", dialogListener).create();
                    dialog.show();
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                    dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                }
                else {
                    for(int i=0;i<form_adapter.getItemCount();i++) {
                        if(form_adapter.getItem(i).isSelected()) { form_adapter.erase(i); i--; } } form_adapter.refresh();
                }
            }
        };

        form_adapter.setOnItemClickListener(new FormAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup parent, View v, int position) {
                if(select_mode) { form_adapter.toggleSelection(position); }
                else {
                    Intent with_intention = new Intent(MainActivity.this, FormActivity.class);
                    with_intention.putExtra(Constants.Data.EXTRA_FORM, form_adapter.getItem(position));
                    form_adapter.deselectAll(); startActivity(with_intention);
                }
            }
        });
        form_adapter.setOnItemLongClickListener(new FormAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick(ViewGroup parent, View v, int position) { form_adapter.toggleSelection(position); }
        });
        form_adapter.addOnItemsPresentListener(new FormAdapter.OnItemsPresentListener() {
            @Override
            public void onItemsPresent(ViewGroup parent) {
                empty_view.setVisibility(View.GONE);
                parent.setVisibility(View.VISIBLE);
            }
            @Override
            public void onNoItemsPresent(ViewGroup parent) {
                empty_view.setVisibility(View.VISIBLE);
                parent.setVisibility(View.GONE);
            }
        });
        form_adapter.addOnItemsSelectedListener(new FormAdapter.OnItemsSelectedListener() {
            @Override
            public void onItemsSelected(ViewGroup parent, int selected_count) {
                select_mode = true; fab_menu.deflate(); //Toast.makeText(getApplicationContext(), selected_count+" form"+(selected_count>1?"s":"")+" selected.", Toast.LENGTH_SHORT).show();
                load.setOnClickListener(deleteListener); load.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override public boolean onLongClick(View view) { form_adapter.selectAll(); deleteListener.onClick(load); return false; }
                });
                load.setImageResource(R.drawable.ic_delete_forever_dark);
            }
            @Override
            public void onNoItemsSelected(ViewGroup parent) {
                select_mode = false; fab_menu.rebind(); load.setImageResource(R.drawable.ic_add_dark);
            }
        });
    }
    private void refreshCachedInformation() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                File[] files = new File(data_model.getFileDirectory()).listFiles();
                /// Delete invalid form files.
                if(files != null) {
                    for(File f : files) {
                        if(f.isFile()) {
                            String file_name = f.toString(); boolean found = false; file_name = file_name.substring(file_name.lastIndexOf('/')+1);
                            for(int i=0;i<form_adapter.getItemCount();i++) { if((form_adapter.getItem(i).getMangledName()+".pdf").equals(file_name)) { found=true; break; } }
                            Log.d(TAG, "| ! | File: "+f.toString()+" does "+(found?"belong to an entry in the database..":"not belong to any entry in the database."));
                            if(!found) {
                                boolean status = f.delete();
                                if(status) Log.d(TAG, "| + | File deleted successfully.");
                                else       Log.d(TAG, "| - | File deletion failed.");
                            }
                        }
                    }
                }
                // Delete cached files.
                File cache = new File(data_model.getFileDirectory()+"/cache");
                if(cache.exists()) {
                    int count = 0; files = cache.listFiles();
                    if(files!=null) for(File file : files) { boolean status = file.delete(); if(status) count++; }
                    Log.d(TAG, "| ! | Cache cleared successfully. "+count+" files deleted.");
                }
                // Refresh IDs based on number of saved forms.
                int id_count = 0;
                for(int i=0;i<form_adapter.getItemCount();i++) {
                    String name = form_adapter.getItem(i).getName();
                    if(Form.getAutoGeneratedId(name)>id_count) id_count = Form.getAutoGeneratedId(name);
                }
                Log.d(TAG, "| ! | UUID for new forms: "+id_count); Form.setIdCount(id_count);
                // Initialize image utility for creating image files.
                ImageUtils.initialize(data_model.getFileDirectory());
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu); return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            default: return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        dark_theme = prefs.getBoolean(Constants.Settings.DARK_THEME, true);
        safe_delete = prefs.getBoolean(Constants.Settings.SAFE_DELETE, true);
        page_size = Integer.parseInt(prefs.getString(Constants.Settings.PAGE_SIZE, "0"));

        setTheme(dark_theme?R.style.AppThemeDark:R.style.AppThemeLight);
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_main);
        themedContext = new ContextThemeWrapper(this, dark_theme?R.style.AppThemeDark:R.style.AppThemeLight);

        metaDbHelper = new MetaDBHelper(this);
        
        data_model = ViewModelProviders.of(this).get(MainViewModel.class);
        if(data_model.getFileDirectory()==null) { File appDirectory = getExternalFilesDir(null); if(appDirectory!=null) { data_model.setFileDirectory(appDirectory.toString()); } else finish(); }
        if(data_model.getFileDirectory()!=null) { Log.d(TAG, "| ^ | File Path: "+data_model.getFileDirectory()); Form.initialize(data_model.getFileDirectory()); }

        /// Setup the toolbar.
        Toolbar toolbar = findViewById(R.id.main_toolbar); toolbar.inflateMenu(R.menu.main_menu); setSupportActionBar(toolbar);

        /// Setup floating action buttons.
        load = findViewById(R.id.btn_load);
        FloatingActionButton load_file = findViewById(R.id.btn_load_file),
                load_camera = findViewById(R.id.btn_load_camera), load_image = findViewById(R.id.btn_load_image);
        
        /// Load the main layout.
        main_layout = findViewById(R.id.base_view);
        main_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override public void onGlobalLayout() { main_layout.getViewTreeObserver().removeOnGlobalLayoutListener(this); fab_menu.inflate(); }
        });
    
        /// Create a floating action button menu.
        fab_menu = new FABMenu(getApplication(), load); fab_menu.add(load_camera, load_file, load_image);
        fab_menu.setOnFABMenuInflatedListener(new FABMenu.OnFABMenuInflatedListener() {
            @Override
            public void onFABMenuInflated(boolean inflated) {
                if(inflated) load.setImageResource(R.drawable.ic_close_dark);
                else load.setImageResource(R.drawable.ic_add_dark);
            }
        });
        fab_menu.setOnFABItemSelectedListener(new FABMenu.OnFABItemSelectedListener() {
            @Override
            public void onFABItemSelected(FABMenuItem item) {
                switch (item.button.getId()) {
                    case R.id.btn_load_camera: {
                        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(i==DialogInterface.BUTTON_POSITIVE) {
                                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, Constants.Requests.CAMERA_LAUNCH_REQUEST);
                                }
                                else if(i==DialogInterface.BUTTON_NEGATIVE) {
                                    Toast.makeText(getApplicationContext(), "Camera Permission Request Denied.",Toast.LENGTH_LONG).show();
                                }
                            }
                        };

                        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setTitle("Camera Permission")
                                    .setMessage("Morform requires permission to access the camera for loading forms.")
                                    .setCancelable(false).setPositiveButton("PROCEED", dialogListener).setNegativeButton("DENY", dialogListener).create();
                            dialog.show();
                            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                            dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                        }
                        else {
                            Intent with_intention = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if(with_intention.resolveActivity(getPackageManager())!=null) {
                                file_path = ImageUtils.createNewImageFile();
                                if(file_path!=null) {
                                    with_intention.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
                                        with_intention.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getApplicationContext(), "com.kv.morform.provider", new File(file_path)));
                                    }
                                    else { with_intention.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file_path))); }
                                    startActivityForResult(with_intention, Constants.Requests.CAMERA_REQUEST);
                                } else {
                                    Snackbar.make(main_layout, "Request could not be initiated. Try again.", Snackbar.LENGTH_SHORT).show();
                                }
                            }
                            else Snackbar.make(main_layout,"No Camera Found",Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    }
                    case R.id.btn_load_file: {
                        Intent with_intention = new Intent(Intent.ACTION_GET_CONTENT);
                        with_intention.setType("application/pdf");
                        startActivityForResult(with_intention, Constants.Requests.FILE_REQUEST);
                        break;
                    }
                    case R.id.btn_load_image: {
                        Intent with_intention = new Intent(Intent.ACTION_GET_CONTENT);
                        with_intention.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        with_intention.setType("image/*");
                        startActivityForResult(Intent.createChooser(with_intention, "Select Image from: "), Constants.Requests.IMAGE_REQUEST);
                        break;
                    }
                }
            }
        });

        empty_view = findViewById(R.id.empty_view);

        /// Setup recycler view and create adapter and respective listeners.
        form_view = findViewById(R.id.form_recycler);
        if(data_model.getAdapter()!=null) form_adapter = data_model.getAdapter();
        else { form_adapter = new FormAdapter(themedContext, dark_theme); setAdapterListeners(); data_model.setAdapter(form_adapter); }
        form_view.setAdapter(form_adapter); form_view.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        refreshCachedInformation();

        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i==DialogInterface.BUTTON_POSITIVE) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.Requests.STORAGE_REQUEST);
                }
                else if(i==DialogInterface.BUTTON_NEGATIVE) {
                    Toast.makeText(getApplicationContext(), "Storage Permission Request Denied.",Toast.LENGTH_LONG).show();
                }
            }
        };

        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            AlertDialog dialog = new AlertDialog.Builder(this).setTitle("Storage Permission")
                    .setMessage("Morform requires permission to read and write to external storage for loading and saving forms.")
                    .setCancelable(false).setPositiveButton("PROCEED", dialogListener).setNegativeButton("DENY", dialogListener).create();
            dialog.show();
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
            dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
        }
    }

    @Override @SuppressWarnings("unchecked")
    public void onActivityResult(int request_code, int result_code, Intent data) {
        if(result_code==RESULT_OK) {
            switch (request_code) {
                case Constants.Requests.FILE_REQUEST: {
                    if(data.getData()!=null) {
                        String file_path = BasicUtils.extractPath(getApplicationContext(), data.getData());
                        if(file_path!=null && !file_path.isEmpty()) {
                            Log.d(TAG, "| ! | Source     : "+file_path);
                            if(BasicUtils.extension(file_path).equalsIgnoreCase("pdf")) {
                                try {
                                    PdfReader reader = new PdfReader(file_path); int page_count = reader.getNumberOfPages();
                                    if(page_count>40) { Snackbar.make(main_layout, "File too large for a form.", Snackbar.LENGTH_SHORT).show(); }
                                    else { addForm(file_path); }
                                } catch (Exception e) { Log.e(TAG, "error: "+e); e.printStackTrace(); }
                            }
                            else { Snackbar.make(main_layout, "Invalid file specified.", Snackbar.LENGTH_SHORT).show(); }
                        }
                    }
                    break;
                }
                case Constants.Requests.CAMERA_REQUEST: {
                    try {
                        if(file_path!=null && !file_path.isEmpty()) {
                            Log.d(TAG, "| ! | Source     : "+file_path);
                            Intent with_intention = new Intent(this, TemporaryImageActivity.class);
                            with_intention.putExtra(Constants.Data.EXTRA_IMAGE, file_path);
                            startActivityForResult(with_intention, Constants.Requests.IMAGE_PROCESS_REQUEST);
                        }
                    }
                    catch (Exception e) { Log.e(TAG, e.toString()); e.printStackTrace(); }
                    break;
                }
                case Constants.Requests.IMAGE_REQUEST: {
                    try {
                        ClipData items = data.getClipData(); ArrayList<String> images = new ArrayList<>();
                        if(data.getData()!=null) {
                            String image_path = ImageUtils.createNewImageFile(), file_path = BasicUtils.extractPath(getApplicationContext(), data.getData());
                            if(image_path!=null && file_path!=null && !file_path.isEmpty() && !image_path.isEmpty()) {
                                Log.d(TAG, "| ! | Source     : "+file_path);
                                Log.d(TAG, "| ! | Destination: "+image_path);
                                if(Arrays.asList("jpg","bmp","png").contains(BasicUtils.extension(file_path).toLowerCase())) {
                                    BasicUtils.copyFile(file_path, image_path); images.add(image_path);
                                } else {
                                    Snackbar.make(main_layout, "Invalid Image file selected.", Snackbar.LENGTH_SHORT).show();
                                    ImageUtils.deleteImageFile(image_path);
                                }
                            }
                        }
                        else if(items!=null) {
                            for(int i=0;i<items.getItemCount();i++) {
                                Log.d(TAG, "| ! | Uri for file "+i+" = "+items.getItemAt(i).getUri());
                                String image_path = ImageUtils.createNewImageFile(), file_path = BasicUtils.extractPath(getApplicationContext(), items.getItemAt(i).getUri());
                                if(image_path!=null && file_path!=null && !file_path.isEmpty() && !image_path.isEmpty()) {
                                    Log.d(TAG, "| ! | Source     : "+file_path);
                                    Log.d(TAG, "| ! | Destination: "+image_path);
                                    if(Arrays.asList("jpg","bmp","png").contains(BasicUtils.extension(file_path).toLowerCase())) {
                                        BasicUtils.copyFile(file_path, image_path); images.add(image_path);
                                    } else {
                                        Snackbar.make(main_layout, getResources().getString(R.string.invalid_image), Snackbar.LENGTH_SHORT).show();
                                        ImageUtils.deleteImageFile(image_path);
                                    }
                                }
                            }
                        }
                        if(!images.isEmpty()) {
                            if(items!=null && images.size()<items.getItemCount())
                                Toast.makeText(getApplicationContext(), R.string.partial_image_loading_failed_prompt, Toast.LENGTH_SHORT).show();
                            Intent with_intention = new Intent(this, TemporaryImageActivity.class);
                            with_intention.putExtra(Constants.Data.EXTRA_IMAGES, images);
                            startActivityForResult(with_intention, Constants.Requests.IMAGE_PROCESS_REQUEST);
                        }
                        else { Snackbar.make(main_layout, getResources().getString(R.string.no_image_prompt), Snackbar.LENGTH_SHORT).show(); }
                    }
                    catch (Exception e) { Log.e(TAG, e.toString()); e.printStackTrace(); }
                    break;
                }
                case Constants.Requests.IMAGE_PROCESS_REQUEST: {
                    Bundle extras = data.getExtras();
                    if(extras!=null) {
                        boolean image_accepted = extras.getBoolean(Constants.Data.EXTRA_SHOULD_SAVE,false);
                        if(image_accepted) {
                            ArrayList<String> images = (ArrayList<String>) data.getExtras().get(Constants.Data.EXTRA_PROCESSED_IMAGES);
                            if(images!=null && images.size()>0) {
                                try {
                                    File pdf_file = new File(data_model.getFileDirectory()+"/cache/save.pdf"); boolean status = true;
                                    if(!pdf_file.exists()) { status = pdf_file.createNewFile(); } else { new PrintWriter(pdf_file).close(); }
                                    if(status) {
                                        Log.d(TAG, "| > | Executing AsyncTask..."); images.add(0, pdf_file.toString());
                                        PDFSaveTask task = new PDFSaveTask(); task.execute(images.toArray(new String[]{}));
                                    } else { Snackbar.make(main_layout, "An error occurred while creating a new form.", Snackbar.LENGTH_SHORT).show(); }
                                } catch (Exception e) { Log.e(TAG, "| X | error: "+e); e.printStackTrace(); }
                            } else { Snackbar.make(main_layout, "An error occurred while saving processed images.", Snackbar.LENGTH_SHORT).show(); }
                        }
                    }
                    break;
                }
            }
        }
        else super.onActivityResult(request_code, result_code, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==Constants.Requests.CAMERA_LAUNCH_REQUEST) {
            for(int i=0;i<permissions.length;i++) {
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED && permissions[i].equals(Manifest.permission.CAMERA)) {
                    Intent with_intention = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(with_intention.resolveActivity(getPackageManager())!=null) {
                        file_path = ImageUtils.createNewImageFile();
                        if(file_path!=null) {
                            with_intention.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
                                with_intention.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getApplicationContext(), "com.kv.morform.provider", new File(file_path)));
                            }
                            else { with_intention.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file_path))); }
                            startActivityForResult(with_intention, Constants.Requests.CAMERA_REQUEST);
                        } else { Snackbar.make(main_layout, "Camera request could not be initiated.", Snackbar.LENGTH_LONG).show(); }
                    }
                    else Snackbar.make(main_layout,"No Camera Found",Snackbar.LENGTH_LONG).show();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void addForm(final String file_path) {
        View new_form_view = LayoutInflater.from(themedContext).inflate(R.layout.dialog_new_form, null);
        final EditText form_name = new_form_view.findViewById(R.id.form_name); form_name.setText(Form.getNewName());
        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i==DialogInterface.BUTTON_POSITIVE) { Form form = new Form(form_name.getText().toString()); form.open(file_path); form_adapter.pushBack(form); }
            }
        };
        final AlertDialog dialog = new AlertDialog.Builder(this).setTitle("New Form...").setView(new_form_view)
                .setCancelable(true).setPositiveButton("ADD", dialogListener).setNegativeButton("CANCEL", dialogListener).create();
        form_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) { if (hasFocus) { dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE); } }
        });
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
    }

    @Override
    public void onBackPressed() {
        if(form_adapter.getSelectedCount()!=0) form_adapter.deselectAll(); else finish();
    }
}