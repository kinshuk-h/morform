package com.kv.morform.activities;

import android.net.Uri;

import androidx.lifecycle.ViewModel;

import com.kv.morform.adapters.FormAdapter;

public class MainViewModel extends ViewModel {
    private String file_directory = null;
    private Uri path;
    private FormAdapter adapter;

    FormAdapter getAdapter() { return adapter; }
    void setAdapter(FormAdapter adapter) { this.adapter = adapter; }

    public Uri getUriPath() { return path; }
    public void setUriPath(Uri path) { this.path=path; }
    public void setFileDirectory(String directory) { file_directory=directory; }
    public String getFileDirectory() { return file_directory; }
}
