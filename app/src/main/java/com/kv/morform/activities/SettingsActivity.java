package com.kv.morform.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.kv.morform.R;
import com.kv.morform.utilities.Constants;

public class SettingsActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==android.R.id.home) { onBackPressed(); }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                if (Constants.Settings.DARK_THEME.equals(s)) {
                    finish(); startActivity(getIntent());
                }
            }
        });

        boolean dark_theme = prefs.getBoolean(Constants.Settings.DARK_THEME, true);
        setTheme(dark_theme?R.style.AppThemeDark:R.style.AppThemeLight);
        setContentView(R.layout.activity_settings);

        getSupportFragmentManager().beginTransaction().replace(R.id.settings, new SettingsFragment()).commit();

        Toolbar toolbar = findViewById(R.id.settings_bar); setSupportActionBar(toolbar);
        ActionBar action_bar = getSupportActionBar();
        if(action_bar!=null) action_bar.setDisplayHomeAsUpEnabled(true);
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.settings_preferences, rootKey);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = NavUtils.getParentActivityIntent(this);
        finish(); if(intent!=null) NavUtils.navigateUpTo(SettingsActivity.this, intent);
    }
}