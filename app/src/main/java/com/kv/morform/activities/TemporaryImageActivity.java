package com.kv.morform.activities;

import android.Manifest;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.snackbar.Snackbar;
import com.kv.morform.BuildConfig;
import com.kv.morform.R;
import com.kv.morform.UI.CropFragment;
import com.kv.morform.UI.ImageFragment;
import com.kv.morform.adapters.ImagePageAdapter;
import com.kv.morform.utilities.BasicUtils;
import com.kv.morform.utilities.Constants;
import com.kv.morform.utilities.ImageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class TemporaryImageActivity extends AppCompatActivity {
    private final static String TAG = TemporaryImageActivity.class.getName();

    private boolean dark_theme = true, safe_delete = true;
    private String image_path;

    private ImagePageAdapter adapter;
    private RelativeLayout main_layout;
    private CropFragment cropFragment;
    private ImageFragment imageFragment;

    void showFailure(boolean should_exit) {
        Toast.makeText(getApplicationContext(), R.string.image_loading_failed_prompt, Toast.LENGTH_SHORT).show(); if(should_exit) onBackPressed();
    }
    void setAdapterListeners() {
        adapter.setAddNewItemListener(new ImagePageAdapter.AddNewItemListener() {
            @Override
            public void addNewItem(String type) {
                switch(type) {
                    case ImagePageAdapter.BTN_CAMERA: {
                        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(i==DialogInterface.BUTTON_POSITIVE) {
                                    ActivityCompat.requestPermissions(TemporaryImageActivity.this, new String[]{Manifest.permission.CAMERA}, Constants.Requests.CAMERA_LAUNCH_REQUEST);
                                }
                                else if(i==DialogInterface.BUTTON_NEGATIVE) {
                                    Toast.makeText(getApplicationContext(), "Camera Permission Request Denied.",Toast.LENGTH_LONG).show();
                                }
                            }
                        };

                        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) {
                            AlertDialog dialog = new AlertDialog.Builder(TemporaryImageActivity.this).setTitle("Camera Permission")
                                    .setMessage("Morform requires permission to access the camera for loading forms.")
                                    .setCancelable(false).setPositiveButton("PROCEED", dialogListener).setNegativeButton("DENY", dialogListener).create();
                            dialog.show();
                            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                            dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                        }
                        else {
                            Intent with_intention = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if(with_intention.resolveActivity(getPackageManager())!=null) {
                                image_path = ImageUtils.createNewImageFile();
                                if(image_path!=null) {
                                    with_intention.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
                                        with_intention.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getApplicationContext(), "com.kv.morform.provider", new File(image_path)));
                                    }
                                    else { with_intention.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(image_path))); }
                                    startActivityForResult(with_intention, Constants.Requests.CAMERA_REQUEST);
                                } else {
                                    Snackbar.make(main_layout, "Request could not be initiated. Try again.", Snackbar.LENGTH_SHORT).show();
                                }
                            }
                            else Snackbar.make(main_layout,"No Camera Found",Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    }
                    case ImagePageAdapter.BTN_IMAGE: {
                        Intent with_intention = new Intent(Intent.ACTION_GET_CONTENT);
                        with_intention.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true); with_intention.setType("image/*");
                        startActivityForResult(Intent.createChooser(with_intention, "Select Image from: "), Constants.Requests.IMAGE_REQUEST);
                        break;
                    }
                }
            }
        });
        adapter.addOnDeleteListener(new ImagePageAdapter.OnDeleteListener() {
            @Override
            public void onDelete(final int position) {
                if(safe_delete) {
                    DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if(i==DialogInterface.BUTTON_POSITIVE) { adapter.erase(position); }
                        }
                    };
                    AlertDialog dialog = new AlertDialog.Builder(TemporaryImageActivity.this)
                            .setTitle("Confirm Delete Page?")
                            .setMessage("You are about to delete page "+(position+1)+". Proceed with deletion?")
                            .setPositiveButton("YES", dialogListener)
                            .setNegativeButton("NO", dialogListener)
                            .setCancelable(true)
                            .create();
                    dialog.show();
                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                    dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
                } else { adapter.erase(position); }
            }
        });
        adapter.setOnItemClickListener(new ImagePageAdapter.OnItemClickListener() {
            @Override public void onItemClick(ViewGroup parent, View v, int position) {
                adapter.select(position); imageFragment.loadImage(adapter.getSelected());
            }
        });
        adapter.addOnEmptyListener(new ImagePageAdapter.OnEmptyListener() {
            @Override public void onEmpty() { finish(); }
        });
    }

    public ImagePageAdapter getAdapter() { return adapter; }
    public String getSelectedImage() {
        return adapter.getSelected();
    }
    public void onCropSuccess() { adapter.setItem(adapter.getSelectedIndex(), cropFragment.saveCrop()); getSupportFragmentManager().popBackStack(); }
    public void onCropFailure() { getSupportFragmentManager().popBackStack(); }
    public void loadCropFragment() {
        try { getSupportFragmentManager().beginTransaction().replace(R.id.fragment_view, cropFragment).addToBackStack(null).commit(); }
        catch (Exception e) { Log.e(TAG, "error: "+e); e.printStackTrace(); }
    }
    public void submit(View view) {
        if(view.getId()==R.id.btn_accept) { getIntent().putExtra(Constants.Data.EXTRA_SHOULD_SAVE,true); getIntent().putExtra(Constants.Data.EXTRA_PROCESSED_IMAGES, adapter.data()); }
        else { getIntent().putExtra(Constants.Data.EXTRA_SHOULD_SAVE, false); } setResult(RESULT_OK, getIntent()); onBackPressed();
    }

    @Override @SuppressWarnings("unchecked")
    public void onCreate(Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        dark_theme = prefs.getBoolean(Constants.Settings.DARK_THEME, true);
        safe_delete = prefs.getBoolean(Constants.Settings.SAFE_DELETE, true);

        this.setTheme(dark_theme?R.style.AppThemeDark:R.style.AppThemeLight);
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_image);

        main_layout = this.findViewById(R.id.main_layout);
        cropFragment = new CropFragment(); imageFragment = new ImageFragment();

        adapter = new ImagePageAdapter(this, dark_theme); setAdapterListeners();

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_view, imageFragment, Constants.IMAGE_FRAGMENT).addToBackStack(null).commit();

        Log.d(TAG, "| * | Initializing "+TAG+"...");

        Bundle data = getIntent().getExtras();
        if(data!=null) {
            ArrayList<String> images = (ArrayList<String>) data.get(Constants.Data.EXTRA_IMAGES); image_path = data.getString(Constants.Data.EXTRA_IMAGE, null);
            if(images!=null) {
                for(String image_path : images) {
                    Log.d(TAG, "| ! | Source: "+image_path);
                    if(!image_path.isEmpty()) {
                        String new_image_path = ImageUtils.getProcessedImage(image_path);
                        if(!new_image_path.isEmpty()) adapter.pushBack(new_image_path);
                    }
                }
                if(adapter.getItemCount()>2) {
                    image_path = adapter.getItem(0); adapter.select(0);
                } else showFailure(true);
            } else if(image_path!=null) {
                String new_image_path = ImageUtils.getProcessedImage(image_path);
                if(!new_image_path.isEmpty()) { adapter.pushBack(new_image_path); adapter.select(0); }
            } else showFailure(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK) {
            switch(requestCode) {
                case Constants.Requests.IMAGE_REQUEST: {
                    try {
                        ClipData items = data.getClipData(); int fail_count = 0;
                        if(data.getData()!=null) {
                            String image_path = ImageUtils.createNewImageFile(), file_path = BasicUtils.extractPath(getApplicationContext(), data.getData()), processed_path;
                            if(image_path!=null && file_path!=null && !file_path.isEmpty() && !image_path.isEmpty()) {
                                Log.d(TAG, "| ! | Source     : "+file_path);
                                Log.d(TAG, "| ! | Destination: "+image_path);
                                if(Arrays.asList("jpg","bmp","png").contains(BasicUtils.extension(file_path).toLowerCase())) {
                                    BasicUtils.copyFile(file_path, image_path); processed_path = ImageUtils.getProcessedImage(image_path); adapter.pushBack(processed_path);
                                    Log.d(TAG, "| > | processed: "+processed_path+" [status="+!processed_path.equals(image_path)+"]");
                                    if(processed_path.equals(image_path)) { showFailure(false); }
                                }
                                else { Snackbar.make(main_layout, getResources().getString(R.string.invalid_image), Snackbar.LENGTH_SHORT).show(); ImageUtils.deleteImageFile(image_path); }
                            }
                        }
                        else if(items!=null) {
                            for(int i=0;i<items.getItemCount();i++) {
                                String image_path = ImageUtils.createNewImageFile(), file_path = BasicUtils.extractPath(getApplicationContext(), items.getItemAt(i).getUri()), processed_path;
                                if(image_path!=null && file_path!=null && !file_path.isEmpty() && !image_path.isEmpty()) {
                                    Log.d(TAG, "| ! | Source     : "+file_path);
                                    Log.d(TAG, "| ! | Destination: "+image_path);
                                    if(Arrays.asList("jpg","bmp","png").contains(BasicUtils.extension(file_path).toLowerCase())) {
                                        BasicUtils.copyFile(file_path, image_path); processed_path = ImageUtils.getProcessedImage(image_path); adapter.pushBack(processed_path);
                                        Log.d(TAG, "| > | processed: "+processed_path+" [status="+!processed_path.equals(image_path)+"]");
                                        if(processed_path.equals(image_path)) { fail_count++; }
                                    } else { ImageUtils.deleteImageFile(image_path); }
                                }
                            }
                        }
                        if(fail_count>0) Toast.makeText(getApplicationContext(), R.string.partial_image_loading_failed_prompt, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e) { Log.e(TAG, e.toString()); e.printStackTrace(); }
                    break;
                }
                case Constants.Requests.CAMERA_REQUEST: {
                    try {
                        String processed_path;
                        if(image_path!=null && !image_path.isEmpty()) {
                            Log.d(TAG, "| ! | Source     : "+image_path);
                            processed_path = ImageUtils.getProcessedImage(image_path); adapter.pushBack(processed_path);
                            if(processed_path.equals(image_path)) { showFailure(false); }
                        }
                    }
                    catch (Exception e) { Log.e(TAG, e.toString()); e.printStackTrace(); }
                    break;
                }
                default: super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override public void onBackPressed() {
        super.onBackPressed();
        if(getSupportFragmentManager().getBackStackEntryCount()<=1) finish();
    }
}
