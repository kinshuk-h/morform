package com.kv.morform.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.pdf.PdfRenderer;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.database.FormDBHelper;
import com.kv.morform.main.Form;
import com.kv.morform.R;
import com.kv.morform.utilities.AndroidUtils;
import com.kv.morform.utilities.BasicUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class FormAdapter extends RecyclerView.Adapter<FormAdapter.ViewHolder> {
    private boolean dark_theme; private int selected_count = 0;
    private static final String TAG = FormAdapter.class.getName();
    private ArrayList<Form> forms; FormDBHelper dbHelper;

    private Context context; private RecyclerView recycler;
    private OnItemClickListener onItemClickListener = new OnItemClickListener() { @Override public void onItemClick(ViewGroup parent, View v, int position) { } };
    private OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() { @Override public void onItemLongClick(ViewGroup parent, View v, int position) { } };
    private OnItemsPresentListener itemsPresentListener = new OnItemsPresentListener() {
        @Override public void onItemsPresent(ViewGroup parent) { } @Override public void onNoItemsPresent(ViewGroup parent) { }
    };
    private OnItemsSelectedListener itemsSelectedListener = new OnItemsSelectedListener() {
        @Override public void onItemsSelected(ViewGroup parent, int count) { } @Override public void onNoItemsSelected(ViewGroup parent) { }
    };

    public interface OnItemClickListener { void onItemClick(ViewGroup parent, View v, int position); }
    public interface OnItemLongClickListener { void onItemLongClick(ViewGroup parent, View v, int position); }
    public interface OnItemsPresentListener { void onItemsPresent(ViewGroup parent); void onNoItemsPresent(ViewGroup parent); }
    public interface OnItemsSelectedListener { void onItemsSelected(ViewGroup parent, int selected_count); void onNoItemsSelected(ViewGroup parent); }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView); recycler = recyclerView; refresh();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        View rootView; TextView name, date, size; ImageView form; ImageView checked;
        ViewHolder(View view){
            super(view); rootView = view;
            name = rootView.findViewById(R.id.name); date = rootView.findViewById(R.id.date);
            form = rootView.findViewById(R.id.form); size = rootView.findViewById(R.id.size);
            checked = rootView.findViewById(R.id.checked);
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(recycler, view, getAdapterPosition());
                }
            });
            rootView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    onItemLongClickListener.onItemLongClick(recycler, view, getAdapterPosition());
                    return false;
                }
            });
        }
    }

    private void reload() {
        if(forms.isEmpty()) { itemsPresentListener.onNoItemsPresent(recycler); } else { itemsPresentListener.onItemsPresent(recycler); }
    }
    private void reloadSelections() {
        if(selected_count>0) { itemsSelectedListener.onItemsSelected(recycler, selected_count); }
        else itemsSelectedListener.onNoItemsSelected(recycler);
    }

    public FormAdapter(Context context, boolean dark_theme) {
        this.dark_theme = dark_theme; this.context=context; this.forms = new ArrayList<>(); reload(); dbHelper = new FormDBHelper(context);
        Cursor cur = dbHelper.data();
        for(cur.moveToFirst();!cur.isAfterLast();cur.moveToNext()) {
            Form form = new Form(cur.getString(cur.getColumnIndex(FormDBHelper.NAME)),cur.getString(cur.getColumnIndex(FormDBHelper.DATE)));
            Log.d(TAG, "Loading form: "+form);
            try { File file = form.getForm(); if(file.length()<=0) { throw new FileNotFoundException(); } pushBack(form); } catch (FileNotFoundException e) { dbHelper.erase(form.getName()); }
        }
    }
    public FormAdapter(Context context, boolean dark_theme, @NonNull ArrayList<Form> forms) {
        this.dark_theme = dark_theme; this.context = context; this.forms = forms; reload(); dbHelper = new FormDBHelper(context);
        /// TODO : Write to DB First.
        Cursor cur = dbHelper.data();
        for(cur.moveToFirst();!cur.isAfterLast();cur.moveToNext()) {
            Form form = new Form(cur.getString(cur.getColumnIndex(FormDBHelper.NAME)),cur.getString(cur.getColumnIndex(FormDBHelper.DATE)));
            try { File file = form.getForm(); if(file.length()<=0) { throw new FileNotFoundException(); } pushBack(form); } catch (FileNotFoundException e) { dbHelper.erase(form.getName()); }
        }
    }

    @Override
    public int getItemCount() { return forms.size(); }

    @Override @NonNull
    public FormAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int view_type) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_form,parent,false));
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int index) {
        Form form = forms.get(holder.getAdapterPosition());
        holder.name.setText(form.getName());
        holder.date.setText(form.getDateCreated());
        Log.d(TAG, "Binding New Form: "+form.toString());
        try {
            File form_file = form.getForm();
            Log.d(TAG, "success: file '"+form.getMangledName()+".pdf' was found.");
            holder.size.setText(BasicUtils.fileSize(form_file.length()));
            try (PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(form_file, ParcelFileDescriptor.MODE_READ_ONLY))) {
                try(PdfRenderer.Page page = renderer.openPage(0)) {
                    Bitmap page_image = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);
                    page.render(page_image, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                    holder.form.setBackgroundColor(Color.argb(255,255,255,255));
                    holder.form.setImageBitmap(page_image);
                }
            }
            catch (Exception e) {
                Log.d(TAG, "error: file '"+form.getMangledName()+".pdf' could not be loaded.");
                Bitmap bitmap = AndroidUtils.toBitmap(context.getResources().getDrawable(dark_theme?R.drawable.ic_form_fill_dark:R.drawable.ic_form_fill_light));
                Bitmap new_bitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), bitmap.getConfig());
                Canvas canvas = new Canvas(new_bitmap);
                canvas.drawColor(Color.parseColor(dark_theme?"#242424":"#DFDFDF"));
                //canvas.drawBitmap(bitmap, 0,0,null);
                holder.form.setBackgroundColor(Color.parseColor(dark_theme?"#242424":"#DFDFDF"));
                holder.form.setImageBitmap(new_bitmap);
            }
        }
        catch(Exception e) {
            Log.d(TAG, "error: file '"+form.getMangledName()+".pdf' was not found."); holder.size.setText("0 B");
            Bitmap bitmap = AndroidUtils.toBitmap(context.getResources().getDrawable(dark_theme?R.drawable.ic_form_fill_dark:R.drawable.ic_form_fill_light));
            Bitmap new_bitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), bitmap.getConfig());
            Canvas canvas = new Canvas(new_bitmap);
            canvas.drawColor(Color.parseColor(dark_theme?"#242424":"#DFDFDF"));
            //canvas.drawBitmap(bitmap, 0,0,null);
            holder.form.setBackgroundColor(Color.parseColor(dark_theme?"#242424":"#DFDFDF"));
            holder.form.setImageBitmap(new_bitmap);
        }
        if(forms.get(holder.getAdapterPosition()).isSelected()) {
            holder.form.setBackground(context.getResources().getDrawable(dark_theme?R.drawable.form_selected_dark:R.drawable.form_selected_light));
            holder.rootView.setBackgroundColor(context.getResources().getColor(dark_theme?R.color.colorAccentDark:R.color.colorAccentLight));
            holder.checked.setVisibility(View.VISIBLE);
            holder.name.setTextColor(context.getResources().getColor(R.color.textColorDark));
            holder.date.setTextColor(context.getResources().getColor(R.color.textColorDark));
            holder.size.setTextColor(context.getResources().getColor(R.color.textColorDark));
        }
        else {
            holder.rootView.setBackground(context.getResources().getDrawable(dark_theme?R.drawable.sharp_card_dark:R.drawable.sharp_card_light));
            holder.checked.setVisibility(View.GONE);
            holder.name.setTextColor(context.getResources().getColor(dark_theme?R.color.textColorDark:R.color.textColorLight));
            holder.date.setTextColor(context.getResources().getColor(dark_theme?R.color.textColorDark:R.color.textColorLight));
            holder.size.setTextColor(context.getResources().getColor(dark_theme?R.color.textColorDark:R.color.textColorLight));
        }
    }

    public void pushBack(Form form) {
        int position = forms.size(); forms.add(form); if(!dbHelper.contains(form.getName())) { dbHelper.insert(form); Log.d(TAG, "Inserted to DB"); }
        else dbHelper.update(form.getName(), form); notifyItemInserted(position); reload(); if(Form.isAutoGeneratedName(form.getName())) Form.refreshIDCount();
    }
    public void insert(int position, Form form) {
        forms.add(position, form); if(!dbHelper.contains(form.getName())) { dbHelper.insert(form); Log.d(TAG, "Inserted to DB"); }
        else dbHelper.update(form.getName(), form); notifyItemInserted(position); reload(); if(Form.isAutoGeneratedName(form.getName())) Form.refreshIDCount();
    }
    public void erase(int position) {
        if(forms.get(position).isSelected()) { selected_count--; reloadSelections(); }
        dbHelper.erase(forms.get(position).getName()); forms.remove(position); notifyItemRemoved(position); reload();
    }
    public Form getItem(int position) { return forms.get(position); }
    public void refresh() { notifyDataSetChanged(); reload(); reloadSelections(); }
    public void toggleSelection(int position) { forms.get(position).toggleSelection(); notifyItemChanged(position);
        if(forms.get(position).isSelected()) { selected_count++; } else { selected_count--; } reloadSelections(); }
    public void deselectAll() { for(int i = 0; i<forms.size(); i++) { if(forms.get(i).isSelected()) toggleSelection(i); } }
    public void selectAll() { for(int i=0;i<forms.size();i++) { if(!forms.get(i).isSelected()) toggleSelection(i); } }
    public int getSelectedCount() { return selected_count; }

    public void setOnItemClickListener(@NonNull OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void setOnItemLongClickListener(@NonNull OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }
    public void addOnItemsPresentListener(@NonNull OnItemsPresentListener itemsPresentListener) {
        this.itemsPresentListener = itemsPresentListener;
    }
    public void addOnItemsSelectedListener(@NonNull OnItemsSelectedListener itemsSelectedListener) {
        this.itemsSelectedListener = itemsSelectedListener;
    }
}
