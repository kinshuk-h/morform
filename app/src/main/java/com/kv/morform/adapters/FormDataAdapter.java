package com.kv.morform.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;

import java.util.ArrayList;

public class FormDataAdapter extends RecyclerView.Adapter<FormDataAdapter.ViewHolder> {
    private boolean dark_theme;
    private static final String TAG = FormDataAdapter.class.getName();
    private ArrayList<Integer> data = new ArrayList<>();

    private Context context; private RecyclerView recycler;
    private OnItemClickListener onItemClickListener = new OnItemClickListener() { @Override public void onItemClick(ViewGroup parent, View v, int position) { } };
    private OnDeleteListener onDeleteListener = new OnDeleteListener() {
        @Override
        public void onDelete(int id) {

        }
    };
    private OnEmptyListener emptyListener;

    public interface OnItemClickListener { void onItemClick(ViewGroup parent, View v, int position); }
    public interface OnDeleteListener { void onDelete(int id); }
    public interface OnEmptyListener { void onEmpty(); }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View root; ImageButton delete; TextView entry;
        public ViewHolder(View v) {
            super(v); root = v; entry = v.findViewById(R.id.form_entry);
            delete = v.findViewById(R.id.btn_delete);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(recycler, root, getAdapterPosition());
                }
            });
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView); recycler = recyclerView;
    }

    @Override
    public int getItemCount() { return data.size(); }

    @Override @NonNull
    public FormDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int view_type) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.form_data_item,parent,false));
    }
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int index) {
        holder.entry.setText(("Form entry #"+data.get(holder.getAdapterPosition())));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                erase(holder.getAdapterPosition());
            }
        });
    }

    public FormDataAdapter(Context context, boolean dark_theme) { this.context=context; this.dark_theme=dark_theme; }

    public void pushBack(int id) {
        int position = data.size(); data.add(id); notifyItemInserted(position);
    }
    public void erase(int position) {
        onDeleteListener.onDelete(data.get(position)); data.remove(position); notifyItemRemoved(position);
        if(getItemCount()==0 && emptyListener!=null) emptyListener.onEmpty();
    }
    public int getItem(int position) { return data.get(position); }

    public void setOnItemClickListener(@NonNull OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public void setOnDeleteListener(@NonNull OnDeleteListener onDeleteListener) {
        this.onDeleteListener = onDeleteListener;
    }
    public void addOnEmptyListener(OnEmptyListener emptyListener) {
        this.emptyListener = emptyListener;
    }
}
