package com.kv.morform.adapters;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;
import com.kv.morform.main.BooleanField;
import com.kv.morform.main.DateField;
import com.kv.morform.main.FormField;
import com.kv.morform.main.Header;
import com.kv.morform.main.OptionField;
import com.kv.morform.main.PlainText;
import com.kv.morform.main.TextField;
import com.kv.morform.utilities.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

public class FormFieldAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private boolean dark_theme;
    private static final String TAG = FormFieldAdapter.class.getName();
    private ArrayList<FormField> fields = new ArrayList<>();

    private Context context; private RecyclerView recycler;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView); recycler = recyclerView;
    }

    public FormFieldAdapter(Context context, boolean dark_theme) {
        this.dark_theme = dark_theme; this.context=context;
    }

    @Override public int getItemCount() {
        int count = 0; boolean count_on = true;
        for(int i=0;i<fields.size();i++) {
            if(fields.get(i) instanceof Header) {
                Header h = (Header) fields.get(i);
                count_on = !h.getState(); count++;
            }
            else if(count_on) count++;
        }
        return count;
    }
    @Override public int getItemViewType(int pos) { return fields.get(pos).getType(); }

    @Override @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int view_type) {
        switch(view_type) {
            case Header.TYPE: return new Header.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.field_header,parent,false));
            case TextField.TYPE: return new TextField.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.field_text,parent,false));
            case OptionField.TYPE: return new OptionField.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.field_option,parent, false));
            case BooleanField.TYPE: return new BooleanField.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.field_boolean,parent,false));
            case DateField.TYPE: return new DateField.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.field_date,parent,false));
            default: return new PlainText.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.field_text_plain, parent, false));
        }
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder rootHolder, final int index) {
        Log.d(TAG, "Index: "+index+", Adapter: "+rootHolder.getAdapterPosition()+", Layout: "+rootHolder.getLayoutPosition());
        FormField rootField = fields.get(rootHolder.getAdapterPosition());
        if(rootField instanceof PlainText) {
            PlainText.ViewHolder holder = (PlainText.ViewHolder) rootHolder;
            PlainText field = (PlainText) rootField;
            holder.text.setText(field.getText());
        }
        else if(rootField instanceof Header) {
            final Header.ViewHolder holder = (Header.ViewHolder) rootHolder;
            final Header field = (Header) rootField;
            holder.header.setText(field.toString());
            Log.d(TAG, "Header!");
            holder.toggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if(field.getState()) {
                            Log.d(TAG, "Header "+field.getPage()+" is being expanded.");
                            holder.toggle.setImageResource(dark_theme?R.drawable.ic_collapse_dark:R.drawable.ic_collapse_light);
                            boolean count_on = true; int i = 0, ct = 0;
                            for(int j=0;j<fields.size();j++) {
                                if(fields.get(j)==field) break;
                                else if(fields.get(j) instanceof Header) {
                                    Header h = (Header) fields.get(j);
                                    count_on = h.getState(); i++;
                                }
                                else if(count_on) i++;
                            }
                            for(int n=i+1;n<fields.size();n++) { if(fields.get(n) instanceof Header) break; else ct++; }
                            Log.d(TAG, "Expanding "+ct+" elements...");
                            Log.d(TAG, "i = "+i+", Index: "+index+", Adapter: "+holder.getAdapterPosition()+", Layout: "+holder.getLayoutPosition());
                            notifyItemRangeInserted(holder.getAdapterPosition()+1, ct);
                        }
                        else {
                            Log.d(TAG, "Header "+field.getPage()+" is being collapsed.");
                            holder.toggle.setImageResource(dark_theme?R.drawable.ic_expand_dark:R.drawable.ic_expand_light);
                            boolean count_on = true; int i = 0, ct = 0;
                            for(int j=0;j<fields.size();j++) {
                                if(fields.get(j)==field) break;
                                else if(fields.get(j) instanceof Header) {
                                    Header h = (Header) fields.get(j);
                                    count_on = h.getState(); i++;
                                }
                                else if(count_on) i++;
                            }
                            for(int n=i+1;n<fields.size();n++) { if(fields.get(n) instanceof Header) break; else ct++; }
                            Log.d(TAG, "Collapsing "+ct+" elements...");
                            Log.d(TAG, "i = "+i+", Index: "+index+", Adapter: "+holder.getAdapterPosition()+", Layout: "+holder.getLayoutPosition());
                            notifyItemRangeRemoved(holder.getAdapterPosition()+1, ct);
                        }
                        field.toggleState();
                    } catch (Exception e) { Log.e(TAG, "| X | error: "+e); e.printStackTrace();
                        Toast.makeText(context, "error: "+e, Toast.LENGTH_LONG).show(); }
                }
            });
        }
        else if(rootField instanceof TextField) {
            final TextField.ViewHolder holder = (TextField.ViewHolder) rootHolder;
            final TextField field = (TextField) rootField;
            holder.label.setText(field.getLabel());
            holder.data.setText(field.getData());
            holder.data.setHint(field.getHint());
            //if(field.getLabel().toLowerCase().contains("rs"))
            //    holder.data.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
            //else if(field.getLabel().toLowerCase().contains("no") )
            //    holder.data.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
            holder.data.addTextChangedListener(new TextWatcher() {
                @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
                    holder.data.setError(null); field.setData(s.toString());
                }
                @Override public void afterTextChanged(Editable s) { }
            });
        }
        else if(rootField instanceof OptionField) {
            final OptionField.ViewHolder holder = (OptionField.ViewHolder) rootHolder;
            final OptionField field = (OptionField) rootField;
            holder.label.setText(field.getLabel());
            holder.options.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, field.getOptions()));
            holder.options.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    field.setSelectedOption(holder.options.getSelectedItem().toString());
                }
                @Override public void onNothingSelected(AdapterView<?> parent) { }
            });
            holder.options.setSelection(field.getOptions().indexOf(field.getSelectedOption()));
        }
        else if(rootField instanceof BooleanField) {
            final BooleanField.ViewHolder holder = (BooleanField.ViewHolder) rootHolder;
            final BooleanField field = (BooleanField) rootField;
            holder.label.setText(field.getLabel());
            holder.state.setChecked(field.getState());
            holder.state.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked!=field.getState()) field.toggle();
                }
            });
        }
        else if(rootField instanceof DateField) {
            final DateField.ViewHolder holder = (DateField.ViewHolder) rootHolder;
            final DateField field = (DateField) rootField;
            holder.label.setText(field.getLabel());
            holder.date.setText(field.getDate());
            holder.date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            field.setDate(DateUtils.getDate(new GregorianCalendar(year, month, dayOfMonth).getTime()));
                            holder.date.setText(field.getDate());
                        }
                    }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                    dialog.show();
                }
            });
        }
    }

    public void pushBack(FormField field) { int position = getItemCount(); fields.add(field); notifyItemInserted(position); }
    public void insert(int position, FormField field) { fields.add(position, field); notifyItemInserted(position); }
    public void erase(int position) { fields.remove(position); notifyItemRemoved(position); }
    public FormField getItem(int position) { return fields.get(position); }
    public ArrayList<FormField> getFields() { return fields; }
    public void clearData() {
        for(FormField rootField : fields) { rootField.clear(); }
        notifyDataSetChanged();
    }
}
