package com.kv.morform.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;
import com.kv.morform.utilities.BasicUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

/// Class to handle processed images to be saved as a PDF.
public class ImagePageAdapter extends RecyclerView.Adapter<ImagePageAdapter.ViewHolder> {
    private boolean dark_theme;
    private ArrayList<String> pages;
    private int selected = 0;
    private static final String TAG = ImagePageAdapter.class.getName();

    public static final String BTN_CAMERA = "C_A_M_E_R_A", BTN_IMAGE = "I_M_A_G_E";

    private Context context; private RecyclerView recycler;
    private OnItemClickListener onItemClickListener = new OnItemClickListener() { @Override public void onItemClick(ViewGroup parent, View v, int position) { } };
    private AddNewItemListener addNewItemListener = new AddNewItemListener() { @Override public void addNewItem(String type) { } };
    private OnDeleteListener onDeleteListener = new OnDeleteListener() { @Override public void onDelete(int position) { } };
    private OnEmptyListener emptyListener = new OnEmptyListener() { @Override public void onEmpty() { } };

    /// Class to handle Item touches and actions, such as swipe to delete or drag to reposition.
    class ItemTouchCallback extends ItemTouchHelper.Callback {
        private final ImagePageAdapter mAdapter;

        ItemTouchCallback(ImagePageAdapter adapter) { mAdapter = adapter; }

        @Override public boolean isLongPressDragEnabled() { return true ; }
        @Override public boolean isItemViewSwipeEnabled() { return false; }

        @Override public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) { }

        @Override public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            return makeMovementFlags(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, 0);
        }
        @Override public boolean onMove(@NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            int fromPosition = viewHolder.getAdapterPosition(), toPosition = target.getAdapterPosition();
            if(fromPosition<mAdapter.getItemCount()-2) {
                String selected_item = mAdapter.getSelected();
                if(toPosition>mAdapter.getItemCount()-3) toPosition=mAdapter.getItemCount()-3;
                if (fromPosition < toPosition) { for (int i = fromPosition; i < toPosition; i++) { Collections.swap(mAdapter.pages, i, i + 1); } }
                else { for (int i = fromPosition; i > toPosition; i--) { Collections.swap(mAdapter.pages, i, i - 1); } }
                mAdapter.notifyItemMoved(fromPosition, toPosition); mAdapter.selected = mAdapter.data().indexOf(selected_item);
            }
            return true;
        }
    }

    /// OnItemClickListener: Handles item clicks.
    public interface OnItemClickListener { void onItemClick(ViewGroup parent, View v, int position); }
    /// AddNewItemListener: Handles item additions via buttons stored beside the pages.
    public interface AddNewItemListener { void addNewItem(String type); }
    /// OnDeleteListener: Handles item deletions (via erase).
    public interface OnDeleteListener { void onDelete(int position); }
    /// OnEmptyListener: Handles states when the adapter has no data (successive calls to erase, etc).
    public interface OnEmptyListener { void onEmpty(); }

    /// ViewHolder for storing views to be recycled.
    class ViewHolder extends RecyclerView.ViewHolder {
        View rootView; ImageView image; ImageButton delete;
        ViewHolder(View root) {
            super(root); this.rootView = root;
            image = root.findViewById(R.id.image); delete = root.findViewById(R.id.btn_delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) { onDeleteListener.onDelete(getAdapterPosition()); }
            });
        }
    }

    /// Triggered when the adapter is attached to a recycler view.
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView); recycler = recyclerView;
        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchCallback(this));
        touchHelper.attachToRecyclerView(recyclerView);
    }

    public ImagePageAdapter(Context context, boolean dark_theme) {
        this.dark_theme = dark_theme; this.context=context; this.pages = new ArrayList<>(); this.pages.add(BTN_CAMERA); this.pages.add(BTN_IMAGE);
    }
    public ImagePageAdapter(Context context, boolean dark_theme, @NonNull ArrayList<String> pages) {
        this.dark_theme = dark_theme; this.context = context; this.pages = pages; this.pages.add(BTN_IMAGE); this.pages.add(BTN_CAMERA);
    }

    /// Returns the current number of pages and buttons in the recycler view.
    @Override public int getItemCount() { return pages.size(); }

    /// Creates a new ViewHolder, which can be recycled in the future.
    @Override @NonNull
    public ImagePageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int view_type) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.image_page, parent, false));
    }

    /// Binds values to the views in the ViewHolder.
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if(pages.get(holder.getAdapterPosition()).equals(BTN_IMAGE)||pages.get(holder.getAdapterPosition()).equals(BTN_CAMERA)) {
            //Log.d(TAG, "Drawing "+pages.get(holder.getAdapterPosition())+" button...");
            holder.delete.setVisibility(View.GONE); holder.image.setBackgroundColor(Color.argb(0,0,0,0));
            holder.rootView.setPadding(0,0,0,0);
            holder.rootView.setOnClickListener(new View.OnClickListener(){
                @Override public void onClick(View v) { addNewItemListener.addNewItem(pages.get(holder.getAdapterPosition())); }
            });
            if(pages.get(holder.getAdapterPosition()).equals(BTN_CAMERA)) {
                holder.image.setImageDrawable(context.getResources().getDrawable(dark_theme?R.drawable.ic_add_camera_image_dark:R.drawable.ic_add_camera_image_light)); }
            else { holder.image.setImageDrawable(context.getResources().getDrawable(dark_theme?R.drawable.ic_add_image_dark:R.drawable.ic_add_image_light)); }
        }
        else {
            holder.delete.setVisibility(View.VISIBLE);
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) { onItemClickListener.onItemClick(recycler, view, holder.getAdapterPosition()); }
            });
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(pages.get(holder.getAdapterPosition()), options);
            holder.image.setImageBitmap(bitmap);
            if(selected==holder.getAdapterPosition()) {
                holder.rootView.setBackground(context.getResources().getDrawable(dark_theme?R.drawable.form_selected_dark:R.drawable.form_selected_light));
                holder.rootView.setPadding(5,5,5,5);
            }
            else {
                holder.rootView.setBackgroundColor(Color.argb(0,0,0,0));
                holder.rootView.setPadding(0,0,0,0);
            }
        }
    }

    /// Adds a new item to the adapter's container, at the end. Items are added before the add buttons.
    public void pushBack(String image) {
        if(!image.isEmpty() && Arrays.asList("jpg","png","bmp").contains(BasicUtils.extension(image).toLowerCase())) {
            Log.d(TAG, "| + | Inserting at position "+(getItemCount()-2)+": "+image);
            pages.add(getItemCount()-2, image); notifyItemInserted(getItemCount()-3);
        }
        if(getItemCount()<=2) { emptyListener.onEmpty(); }
    }
    /// Selects the entry referred by the given position, and deselects the previous entry.
    public void select(int position) {
        if(position<getItemCount()-2 && selected!=position) {
            Log.d(TAG, "| ! | Old selection: "+selected); Log.d(TAG, "| ! | New selection: "+position);
            int old_pos = selected; selected = position; notifyItemChanged(old_pos); if(position!=-1) { notifyItemChanged(position); }
        } else if(selected==position) { Log.d(TAG, "| ! | Old selection same as new selection."); }
    }
    /// Deletes the entry referred by the given position. Also selects a new entry if the deleted entry was selected.
    public void erase(int position) {
        if(position<getItemCount()-2) {
            ViewHolder holder = (ImagePageAdapter.ViewHolder) recycler.findViewHolderForAdapterPosition(position>0?position-1:position+1);
            if(holder!=null) onItemClickListener.onItemClick(recycler, holder.rootView, holder.getAdapterPosition());
            pages.remove(position); notifyItemRemoved(position);
        }
        if(getItemCount()<=2) { emptyListener.onEmpty(); }
    }
    /// Returns the item at given position.
    public String getItem(int position) { return pages.get(position); }
    /// Sets the value of the item at given position to a new one.
    public void setItem(int position, String value) { pages.set(position, value); notifyItemChanged(position); }
    /// Returns an ArrayList of data entries, without the extra buttons.
    public ArrayList<String> data() { return new ArrayList<>(pages.subList(0, pages.size()-2)); }
    /// Returns the currently selected item.
    public String getSelected() { return pages.get(selected); }
    /// Returns the index of the currently selected item.
    public int getSelectedIndex() { return selected; }

    /// Sets the OnItemClickListener of the adapter (Triggered upon item click)
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) { this.onItemClickListener = onItemClickListener; }
    /// Sets the AddNewItemListener of the adapter (Triggered upon item addition using image and camera buttons)
    public void setAddNewItemListener(AddNewItemListener addNewItemListener) { this.addNewItemListener = addNewItemListener; }
    /// Adds a new OnDeleteListener (Triggered upon item deletion)
    public void addOnDeleteListener(OnDeleteListener onDeleteListener) { this.onDeleteListener = onDeleteListener; }
    /// Adds a new OnEmptyListener (Triggered when the list is empty)
    public void addOnEmptyListener(OnEmptyListener emptyListener) { this.emptyListener = emptyListener; }
}
