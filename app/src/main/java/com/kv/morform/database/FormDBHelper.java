package com.kv.morform.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kv.morform.main.Form;

public class FormDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE = "forms.db", FORMS = "forms", ID = "ID", NAME = "NAME", IDENTIFIER = "IDENTIFIER", DATE = "DATE";
    private static final String TAG = FormDBHelper.class.getName();
    public static int DB_VERSION = 1;
    public FormDBHelper(Context appContext) {
        super(appContext,DATABASE,null,DB_VERSION); Log.d(TAG, "Initializing DB");
    }
    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+FORMS+"("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+NAME+" TEXT, "+IDENTIFIER+" TEXT, "+DATE+" TEXT)");
    }
    @Override public void onUpgrade(SQLiteDatabase db, int old_ver, int new_ver) {
        db.execSQL("DROP TABLE IF EXISTS "+FORMS); onCreate(db);
    }
    public boolean contains(String column, String query) { Cursor res = find(column, query); res.moveToFirst(); return res.getCount()>0; }
    public boolean contains(String name) { return contains(FormDBHelper.NAME, name); }
    public Cursor find(String column, String query) { return getReadableDatabase().rawQuery("SELECT * FROM "+FORMS+" WHERE "+column+"=?", new String[]{query}); }
    public Cursor findSimilar(String column, String query) { return getReadableDatabase().rawQuery("SELECT * FROM "+FORMS+" WHERE "+column+" LIKE ?", new String[]{query}); }
    public Cursor data() { return getReadableDatabase().rawQuery("SELECT * FROM "+FORMS, null); }
    public void insert(Form form) {
        ContentValues values = new ContentValues();
        values.put(NAME, form.getName());
        values.put(IDENTIFIER, form.getMangledName());
        values.put(DATE, form.getDateCreated());
        getWritableDatabase().insert(FORMS, null, values);
    }
    public void update(String name, Form form) {
        ContentValues values = new ContentValues();
        values.put(NAME, form.getName());
        values.put(IDENTIFIER, form.getMangledName());
        values.put(DATE, form.getDateCreated());
        getWritableDatabase().update(FORMS, values, NAME+"=?", new String[]{ name });
    }
    public void erase(String name) {
        getWritableDatabase().delete(FORMS, NAME+"=?", new String[]{ name });
    }
}
