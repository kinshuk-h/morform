package com.kv.morform.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MetaDBHelper extends SQLiteOpenHelper {
    private static final String TABLES = "RootTable", TABLE_NAME = "table_name", DATABASE = "all_form_data.db";
    private static final int VERSION = 2;

    public MetaDBHelper(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLES+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, "+TABLE_NAME+" TEXT)");
    }

    @Override public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLES); onCreate(db);
    }

    Cursor getTables() { return getReadableDatabase().rawQuery("SELECT "+TABLE_NAME+" FROM "+TABLES, null); }
    public void addTable(String table, String properties) {
        ContentValues values = new ContentValues(); values.put(TABLE_NAME, table);
        getWritableDatabase().insert(TABLES, null, values);
        getWritableDatabase().execSQL("CREATE TABLE IF NOT EXISTS "+table+"("+properties+")");
    }
    public void deleteTable(String table) {
        getWritableDatabase().execSQL("DROP TABLE IF EXISTS "+table);
        getWritableDatabase().delete(TABLES,TABLE_NAME+"=?", new String[]{table});
    }
    public boolean findTable(String table) {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM "+TABLES+" WHERE "+TABLE_NAME+"=?", new String[]{table});
        int rows = cursor.getCount(); cursor.close(); return rows>0;
    }
    public void insertData(String table, ContentValues values) { getWritableDatabase().insert(table, null, values); }
    public Cursor getData(String table) { return getReadableDatabase().rawQuery("SELECT * FROM "+table, null); }
    public Cursor getData(String table, int id) { return getReadableDatabase().rawQuery("SELECT * FROM "+table+" WHERE __ID__==?", new String[]{String.valueOf(id)}); }
    public void deleteData(String table, int id) { getWritableDatabase().delete(table, "__ID__=?", new String[]{String.valueOf(id)}); }
}
