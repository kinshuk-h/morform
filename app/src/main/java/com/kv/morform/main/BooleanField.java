package com.kv.morform.main;

import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;

public class BooleanField extends FormField {
    private String label; private boolean state = false;

    public static final int TYPE = 5;
    public int getType() { return TYPE; }
    public void clear() { state=false; }

    public BooleanField() {}
    public BooleanField(String label) { this.label = label; }
    public BooleanField(String label, boolean state) { this.label = label; this.state = state; }

    public String getLabel() { return label; }
    public boolean getState() { return state; }

    public void setLabel(String label) { this.label=label; }
    public void toggle() { state=!state; }

    @NonNull public String toString() { return String.valueOf(state); }
    public  void fromString(String string) { state = Boolean.parseBoolean(string); }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root; public TextView label; public Switch state;
        public ViewHolder(View rootView) {
            super(rootView); root = rootView; label = root.findViewById(R.id.label); state=root.findViewById(R.id.state);
        }
    }
}
