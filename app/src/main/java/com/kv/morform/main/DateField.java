package com.kv.morform.main;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;
import com.kv.morform.utilities.DateUtils;

import java.util.Calendar;

public class DateField extends FormField {
    private String label, date = "";

    public final static int TYPE = 7;
    public int getType() { return TYPE; }
    public void clear() { date = ""; }

    public DateField() {}
    public DateField(String label) { this.label = label; }
    public DateField(String label, String date) { this.label = label; this.date = date; }

    public String getLabel() { return label; }
    public String getDate() { return date.isEmpty()? DateUtils.getDate(Calendar.getInstance().getTime()):date; }

    public void setDate(String date) { this.date=date; }
    public void setLabel(String label) { this.label=label; }

    @NonNull public String toString() { return date; }
    public  void fromString(String string) { date = string; }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root; public TextView label; public TextView date;
        public ViewHolder(View rootView) {
            super(rootView); root = rootView; label = root.findViewById(R.id.label); date=root.findViewById(R.id.date);
        }
    }
}
