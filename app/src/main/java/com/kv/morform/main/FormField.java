package com.kv.morform.main;

import androidx.annotation.NonNull;

public abstract class FormField {
    public abstract int getType();
    public abstract void clear();
    @NonNull  public abstract String toString();
    public abstract void fromString(String string);
    public String getLabel() { return ""; }
}
