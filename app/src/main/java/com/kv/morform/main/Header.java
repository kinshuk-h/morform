package com.kv.morform.main;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;

public class Header extends FormField {
    private boolean state = false; private int page_no;

    public final static int TYPE = 2;
    public int getType() { return TYPE; }
    public void clear() { }

    public Header(int page_no) { this.page_no = page_no; }
    @NonNull public String toString() { return "Page #"+page_no; }
    public int getPage() { return page_no; }
    public boolean getState() { return state; }
    public void toggleState() { state=!state; }

    public  void fromString(String string) { }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root; public TextView header; public ImageButton toggle;
        public ViewHolder(View rootView) {
            super(rootView); root = rootView; header = root.findViewById(R.id.header); toggle=root.findViewById(R.id.toggle_header);
        }
    }
}
