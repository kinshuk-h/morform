package com.kv.morform.main;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetaData {
    private String label, type = "TextField", data;
    private int line_no, size, col_no;

    public MetaData() { }
    public MetaData(String label, int line_no, int col_no, int size) { this.size=size; this.label=label; this.line_no=line_no; this.col_no=col_no; }

    public int getLine() { return line_no; }
    public int getColumn() { return col_no; }
    public int getSize() { return size; }
    public String getData() { return data; }
    public String getType() { return type; }
    public String getLabel() { return label; }

    public void setLine(int line_no) { this.line_no=line_no; }
    public void setColumn(int col_no) { this.col_no=col_no; }
    public void setSize(int size) { this.size=size; }
    public void setData(String data) { this.data=data; }
    public void setType(String type) { this.type=type; }
    public void setLabel(String label) { this.label=label; }

    @NonNull
    public String toString() {
        return "Line "+line_no+", Size. "+size+", Col. "+col_no+": "+label+" ( "+type+" ) > "+data;
    }

    public void normalize () {
        label = label.trim();
        Log.d(Form.class.getName(), "Label: \""+label+"\" (Len="+label.length()+")");
        Pattern option = Pattern.compile("([({\\[])((\\s*[A-Z0-9a-z][A-Z0-9a-z. ]*[,\\/|]*)+)([)}\\]])");
        Pattern red_option = Pattern.compile("((\\s*[A-Z0-9a-z][A-Z0-9a-z. ]*[\\/|]*)+)");
        Matcher m = option.matcher(label); Matcher m2 = red_option.matcher(label);
        while(m.find()) {
            if((m.group(0).contains(",") || m.group(0).contains("/"))) {
                data = m.group(0).replace("(","").replace(")","".replace("{","".replace("}",""))); type = "OptionField";
                String[] pieces = data.split("[/,]");
                if(pieces.length==2) {
                    for(String piece : pieces) {
                        if(piece.toLowerCase().contains("yes")||piece.contains("true")) { type = "BooleanField"; }
                    }
                }
                label = label.replace(m.group(0), "");
                label = label.trim(); break;
            }
        }
        if(type.equals("TextField")) {
            while(m2.find()) {
                if(m2.group(0).split("[\\/]").length>2) {
                    data = m2.group(0); type = "OptionField";
                    String[] pieces = data.split("[/,]");
                    if(pieces.length==2) {
                        for(String piece : pieces) {
                            if(piece.toLowerCase().contains("yes")||piece.contains("true")) { type = "BooleanField"; }
                        }
                    }
                    label = label.replace(m2.group(0), "");
                    label = label.trim(); break;
                }
            }
        }
        if(type.equals("TextField")) {
            int i = label.toLowerCase().indexOf("date");
            if(i>0&&i<label.length()-4) {
                if(label.charAt(i-1)==' '&&label.charAt(i+4)==' ') type="DateField";
            }
            else if(label.equalsIgnoreCase("date")||label.toLowerCase().contains("date")&&label.length()==4) type="DateField";
            else if(i==0) { if(label.charAt(i+4)==' ') type="DateField"; }
            else if(i==label.length()-4) { if(label.charAt(i-1)==' ') type="DateField"; }
            else {
                i = label.toLowerCase().indexOf("dated");
                if(label.equalsIgnoreCase("dated")||label.toLowerCase().contains("dated") && label.length()==5) type="DateField";
                if(i>0&&i<label.length()-5) {
                    if(label.charAt(i-1)==' '&&label.charAt(i+5)==' ') type="DateField";
                }
                else if(i==0) { if(label.charAt(i+5)==' ') type="DateField"; }
                else if(i==label.length()-5) { if(label.charAt(i-1)==' ') type="DateField"; }
            }
        }
    }
}
