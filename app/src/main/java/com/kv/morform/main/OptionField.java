package com.kv.morform.main;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;

import java.util.ArrayList;

public class OptionField extends FormField {
    private ArrayList<String> options;
    private String label, option;

    public static final int TYPE = 4;
    public int getType() { return TYPE; }
    public void clear() { if(options.size()>0) { option = options.get(0); } }

    public OptionField() {}
    public OptionField(String label) { this.label=label; }
    public OptionField(String label, @NonNull ArrayList<String> options) { this.label=label; this.options=options; if(options.size()>0) option=options.get(0); }

    public void setLabel(String label) { this.label=label; }
    public void setSelectedOption(String option) { this.option = option; }
    public void setOptions(@NonNull ArrayList<String> options) { this.options = options; if(options.size()>0) option=options.get(0);  }

    public String getSelectedOption() { return option; }
    public String getLabel() { return label; }
    public ArrayList<String> getOptions() { return options; }

    @NonNull public String toString() { return option; }
    public  void fromString(String string) { option = string; }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root; public TextView label; public Spinner options;

        public ViewHolder(View rootView) {
            super(rootView); root = rootView; label = root.findViewById(R.id.label); options = root.findViewById(R.id.option);
        }
    }
}
