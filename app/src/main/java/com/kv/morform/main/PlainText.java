package com.kv.morform.main;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;

public class PlainText extends FormField {
    String text;

    public final static int TYPE = 1;
    public int getType() { return TYPE; }
    public void clear() { }

    public PlainText() { text = null; }
    public PlainText(String text) { this.text = text; }

    public String getText() { return text; }
    public void setText(String text) { this.text=text; }

    @NonNull public String toString() { return ""; }
    public  void fromString(String string) { }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root; public TextView text;
        public ViewHolder(View rootView) {
            super(rootView); root = rootView; text = root.findViewById(R.id.text);
        }
    }
}
