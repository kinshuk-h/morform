package com.kv.morform.main;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kv.morform.R;

public class TextField extends FormField {
    private String label, data;

    public final static int TYPE = 3;
    public int getType() { return TYPE; }
    public void clear() { data = ""; }

    public TextField() {}
    public TextField(String label) { this.label = label; }
    public TextField(String label, String data) { this.label = label; this.data = data; }

    public String getHint() { return "Add "+label+" Data"; }
    public String getLabel() { return label; }
    public String getData() { return data; }

    public void setData(String data) { this.data=data; }
    public void setLabel(String label) { this.label=label; }

    @NonNull public String toString() { return data; }
    public  void fromString(String string) { data = string; }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View root; public TextView label; public EditText data;
        public ViewHolder(View rootView) {
            super(rootView); root = rootView; label = root.findViewById(R.id.label); data=root.findViewById(R.id.data);
        }
    }
}
