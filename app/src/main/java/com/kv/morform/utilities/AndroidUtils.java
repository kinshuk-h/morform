package com.kv.morform.utilities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import androidx.core.graphics.ColorUtils;

import java.util.NoSuchElementException;

public class AndroidUtils {
    private static final String TAG = AndroidUtils.class.getName();
    public static Bitmap toBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
    public static void scaleToFit(ImageView view) throws NoSuchElementException {
        Bitmap bitmap;
        try { Drawable drawing = view.getDrawable(); bitmap = ((BitmapDrawable) drawing).getBitmap(); }
        catch (NullPointerException e) { throw new NoSuchElementException("No drawable on given view"); }

        // Get current dimensions AND the desired bounding box
        int width;
        try { width = bitmap.getWidth(); }
        catch (NullPointerException e) { throw new NoSuchElementException("Can't find bitmap on given view/drawable"); }

        int height = bitmap.getHeight();
        int bounding = 250;
        Log.i(TAG, "original width = "+width);
        Log.i(TAG, "original height = "+height);
        Log.i(TAG, "bounding = "+bounding);

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;
        Log.i(TAG, "xScale = " + (xScale));
        Log.i(TAG, "yScale = " + (yScale));
        Log.i(TAG, "scale = " + (scale));

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        Log.i(TAG, "scaled width = " + (width));
        Log.i(TAG, "scaled height = " + (height));

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        Log.i(TAG, "done");
    }
    public static int getTextColor(int bgcolor) {
        if(ColorUtils.calculateLuminance(bgcolor)<0.5) return Color.argb(255,240,240,240);
        else return Color.argb(255,0,0,0);
    }
}
