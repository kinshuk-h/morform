package com.kv.morform.utilities;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.documentfile.provider.DocumentFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

public class BasicUtils {
    private static final String TAG = BasicUtils.class.getName();
    private static String base_path;

    private static boolean isExternalStorageDocument(Uri uri) { return "com.android.externalstorage.documents".equals(uri.getAuthority()); }
    private static boolean isDownloadsDocument(Uri uri)       { return "com.android.providers.downloads.documents".equals(uri.getAuthority()); }
    private static boolean isMediaDocument(Uri uri)           { return "com.android.providers.media.documents".equals(uri.getAuthority()); }
    private static boolean isGooglePhotosUri(Uri uri)         { return "com.google.android.apps.photos.content".equals(uri.getAuthority()); }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        final String column = "_data"; final String[] projection = { column };
        try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null)) {
            if (cursor != null && cursor.moveToFirst()) { final int index = cursor.getColumnIndexOrThrow(column); return cursor.getString(index); }
        } catch (Exception e) { Log.e(TAG, "error: "+e); e.printStackTrace(); }
        return null;
    }

    public void setBasePath(String path) { base_path = path; }

    public static String extractPath(final Context context, final Uri uri) {
        Log.d(TAG, "| ? | Base URI: "+uri.toString());
        if (DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                Log.d(TAG, "| ! | URI type: 'content', and links to a file from external storage.");
                final String docId = DocumentsContract.getDocumentId(uri); final String[] part = docId.split(":");
                if ("primary".equalsIgnoreCase(part[0])) { return Environment.getExternalStorageDirectory() + "/" + part[1];  }
            }
            else if (isDownloadsDocument(uri)) {
                Log.d(TAG, "| ! | URI type: 'content', and links to a file from downloads.");
                final String id = DocumentsContract.getDocumentId(uri);
                Log.d(TAG, "| ? | Fetched ID: "+id);
                if (!TextUtils.isEmpty(id)) {
                    if (id.startsWith("raw:")) { return id.replaceFirst("raw:", ""); }
                    try {
                        String[] prefixes = new String[]{ "content://downloads/public_downloads", "content://downloads/my_downloads", "content://downloads/all_downloads" };
                        for(String prefix : prefixes) {
                            Uri content_uri = ContentUris.withAppendedId(Uri.parse(prefix), Long.valueOf(id));
                            String path = getDataColumn(context, content_uri, null, null); if(path!=null) { return path; }

                        }
                    } catch (Exception e) { Log.e(TAG, "error: "+e); }
                } return null;
            }
            else if (isMediaDocument(uri)) {
                Log.d(TAG, "| ! | URI type: 'content', and links to a file from a media source.");
                final String docId = DocumentsContract.getDocumentId(uri); final String[] part = docId.split(":"); Uri contentUri = null;
                if ("image".equals(part[0]))      { contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI; }
                else if ("video".equals(part[0])) { contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI; }
                else if ("audio".equals(part[0])) { contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; }
                return getDataColumn(context, contentUri, "_id=?", new String[] { part[1] });
            }
        }
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri)) { Log.d(TAG, "| ! | URI type: 'content', and points to a file from Google Photos."); return uri.getLastPathSegment(); }
            Log.d(TAG, "| ! | URI type: 'content', and points indirectly to a probable file."); return getDataColumn(context, uri, null, null);
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) { Log.d(TAG, "| ! | URI type: 'file', and points directly to a file."); return uri.getPath(); }
        return null;
    }
    public static String getAccessiblePath(Context context, Uri uri) {
        if(base_path==null) return null;
        try (InputStream in = context.getContentResolver().openInputStream(uri);
             FileOutputStream out = new FileOutputStream(base_path+"/tmp.")) {
            return "hello";
        } catch (Exception e) { Log.e(TAG, "error: "+e); e.printStackTrace(); return null; }
    }

    public static String fileSize(long bytes) {
        int index = 0; double size = bytes;
        String[] units = new String[]{"B","KB","MB","GB","TB","PB"};
        while(size>1024.0) { size/=1024.0; index++; }
        return new DecimalFormat("0.###").format(size) +" "+units[index];
    }
    public static void copyFile(String src, String dest) {
        try(InputStream is = new FileInputStream(src); OutputStream os = new FileOutputStream(dest)) {
            byte[] buffer = new byte[1024]; int length;
            while ((length = is.read(buffer)) > 0) { os.write(buffer, 0, length); }
        } catch (IOException e) { Log.d(BasicUtils.class.getName(), "error: "+e); e.printStackTrace(); }
    }
    public static String extension(String path) {
        int pos = path.lastIndexOf('.'); if(pos==-1) return "";
        return path.substring(pos+1);
    }
    public static String getMangledName(String name) {
        StringBuilder builder=new StringBuilder();
        name = name.toLowerCase().replace(" ", "_");
        for(int i=0;i<name.length();i++) {
            if ((name.charAt(i) >= 'A' && name.charAt(i) <= 'Z') || (name.charAt(i) >= 'a' && name.charAt(i) <= 'z') || name.charAt(i) == '_') {
                builder.append(name.charAt(i));
            }
        }
        return builder.toString();
    }
}
