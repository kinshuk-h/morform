package com.kv.morform.utilities;

import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;

public interface Constants {
    /// Constants to serve as request codes.
    interface Requests {
        int CAMERA_REQUEST = 691, FILE_REQUEST = 692, IMAGE_REQUEST = 693,
            CAMERA_LAUNCH_REQUEST = 694, STORAGE_REQUEST = 695, IMAGE_PROCESS_REQUEST = 696, SAVE_REQUEST = 697;
    }
    /// Constants to serve as keys for transferring data via intents.
    interface Data {
        String EXTRA_IMAGES = "selected_images", EXTRA_PROCESSED_IMAGES = "processed_images",
               EXTRA_SHOULD_SAVE = "images_accepted", EXTRA_IMAGE = "selected_image", EXTRA_FORM = "open_form";
    }
    /// Constants representing keys of settings.
    interface Settings {
        String DARK_THEME = "dark_theme", SAFE_DELETE = "safe_delete", PAGE_SIZE = "page_size";
    }
    /// Constants for use as TAGs in Fragments.
    String IMAGE_FRAGMENT = "image_fragment", CROP_FRAGMENT = "crop_fragment";
    Rectangle[] pages = new Rectangle[]{ PageSize.A4, PageSize.A4.rotate(), PageSize.A5, PageSize.LETTER };
}
