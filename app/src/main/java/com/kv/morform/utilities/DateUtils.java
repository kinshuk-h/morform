package com.kv.morform.utilities;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static String TAG = DateUtils.class.getName();
    public static Date toDate(String date) {
        try {
            if(date==null || date.isEmpty()) { return Calendar.getInstance().getTime(); }
            return SimpleDateFormat.getDateInstance().parse(date);
        }
        catch (ParseException e)  {
            Log.e(TAG, "error: "+e); e.printStackTrace();
            return Calendar.getInstance().getTime();
        }
    }
    public static String getLongDate(Date day) {
        return new SimpleDateFormat("MMMM dd, yyyy", Locale.US).format(day);
    }
    public static String getDate(Date day) { return SimpleDateFormat.getDateInstance().format(day); }
    public static String getLongMonthAndYear(Date day) { return new SimpleDateFormat("MMMM, yyyy", Locale.US).format(day); }
    public static String get(String format, Date day) { return new SimpleDateFormat(format, Locale.US).format(day); }
    public static String getFormat() { return ((SimpleDateFormat)(SimpleDateFormat.getDateInstance())).toPattern(); }
}
