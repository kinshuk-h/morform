package com.kv.morform.utilities;

import android.graphics.Point;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ImageUtils {
    private static String root_directory;
    private static final String TAG = ImageUtils.class.getName();
    private static int file_count = 0;

    public static void initialize(String directory) {
        root_directory = directory;
        File cache = new File(root_directory+"/cache");
        if(!cache.exists()) {
            boolean status = cache.mkdir();
            if(status)  Log.d(TAG, "| + | Cache directory creation successful.");
            else        Log.d(TAG, "| - | Cache directory creation failed.");
        } else {
            File[] files = cache.listFiles();
            if(files!=null) file_count = files.length;
        }
    }
    public static String createNewImageFile() {
        if(root_directory!=null) {
            try {
                File image = new File(root_directory+"/cache/img_"+file_count+".jpg"); boolean status = image.createNewFile();
                if(status) { Log.d(TAG, "| + | Image file creation successful."); file_count++; return image.toString(); }
                else       { Log.d(TAG, "| - | Image file creation failed."); return null; }
            } catch (IOException e) {
                Log.e(TAG, e.toString()); e.printStackTrace(); return null;
            }
        } return null;
    }
    public static String getProcessedImage(String path) {
        String new_path = processImage(path);
        if(new_path!=null && !new_path.isEmpty()) { Log.d(TAG, "| + | Image processing successful."); return new_path; }
        else                                      { Log.d(TAG, "| - | Image processing failed."); return path; }
    }
    public static String getProcessedImage(String path, ArrayList<Point> contour) {
        String new_path = processImage(path, contour);
        if(new_path!=null && !new_path.isEmpty()) { Log.d(TAG, "| + | Image processing successful."); return new_path; }
        else                                      { Log.d(TAG, "| - | Image processing failed."); return path; }
    }
    public static void deleteImageFile(String image_path) {
        File file = new File(image_path);
        if(file.exists()) {
            boolean status = file.delete();
            if(status) { Log.d(TAG, "| + | Image file deleted successfully."); file_count--; }
            else       { Log.d(TAG, "| - | Image file deletion failed."); }
        }
    }
    public static String getUnprocessed(String processed_image) {
        return processed_image.replace("_processed", "");
    }

    private static native String processImage(String path);
    private static native String processImage(String path, ArrayList<Point> contour);

    public  static native void adjustRatio(String path, double ratio);
    public static native void rotate(String path, double angle);
    public static native ArrayList<Point> getContour(String path);
}
