package com.kv.morform.utilities;

import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;

public class LocationTextStrategy extends LocationTextExtractionStrategy
{
    @Override
    protected boolean isChunkAtWordBoundary(TextChunk chunk, TextChunk previousChunk) {
        float dist = chunk.distanceFromEndOf(previousChunk);
        if(dist < -chunk.getCharSpaceWidth() || dist > chunk.getCharSpaceWidth() / 6.0f) return true;
        return false;
    }
}